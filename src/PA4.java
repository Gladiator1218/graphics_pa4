//****************************************************************************
//			CS680 Programming Assignment 4
//****************************************************************************
// Description: 
//   Originated from Example Main Program for CS480 PA1
//   This is a template program for the sketching tool.  
//   The instructor will be printed at the beginning of the program
//****************************************************************************
// History :
//   Aug 2004 Created by Jianming Zhang based on the C
//   code by Stan Sclaroff
//  Nov 2014 modified to include test cases for shading example for PA4
//  Nov 2015 modified by Renqing Gao to implement Programming assignment 4
//****************************************************************************
// reference:
//		from wiki and class textbook

import javax.swing.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.*;
import java.awt.image.*;
//import java.io.File;
//import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

//import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.awt.GLCanvas;//for new version of gl
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;

import com.jogamp.opengl.util.FPSAnimator;//for new version of gl

import Lights.*;
import Objects.*;
import Test.TestCases;
import Utils.*;

public class PA4 extends JFrame implements GLEventListener, KeyListener, MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;
	private final int DEFAULT_WINDOW_WIDTH = 512;
	private final int DEFAULT_WINDOW_HEIGHT = 512;
	private final float DEFAULT_LINE_WIDTH = 1.0f;

	private GLCapabilities capabilities;
	private GLCanvas canvas;
	private FPSAnimator animator;

	final private int numTestCase;
	private int testCaseID;
	private BufferedImage buff;
	private float[][] depthBuffer;
	@SuppressWarnings("unused")
	private ColorType color;
	private Random rng;
	
	//boolean switches
    public boolean ambientTermSwitch = true;
    public boolean diffuseTermSwitch = true;
    public boolean specularTermSwitch = true;
    public boolean ambientSwitch = true;
    public boolean infiniteSwitch = true;
    public boolean pointSwitch = true;
    public boolean spotSwitch = true;
    public boolean translateSwitch = false;
    public boolean case4RotateSwitch = false;
    public boolean scaleSwitch = false;
    public boolean doSmoothShading = false;
    public boolean doPhongShading = false;
    public boolean cameraRotateSwitch = false;
    public boolean cameraTranslateSwitch = false;
    /** Whether the world is being rotated. */
    private boolean rotate_world = false;
	//test using
	private TestCases testCases;
	// specular exponent for materials
	private int ns = 5;
	private int materialNum = 8;
	public float scaleParam = 1f;
	private int materialOffset = 0;
	private ArrayList<Point3D> lineSegs;
	private ArrayList<Point3D> triangles;
	private int Nsteps;
	public Vector3D centerOffset = new Vector3D();

	/** The quaternion which controls the rotation of the world. */
	private Quaternion viewing_quaternion = new Quaternion();
	private Quaternion rotateQuaternion = new Quaternion();
	private Vector3D viewing_center = new Vector3D((float) (DEFAULT_WINDOW_WIDTH / 2),
			(float) (DEFAULT_WINDOW_HEIGHT / 2), (float) 0.0);
	/** The last x and y coordinates of the mouse press. */
	private int last_x = 0, last_y = 0;
	
	

	public PA4() {
		capabilities = new GLCapabilities(null);
		capabilities.setDoubleBuffered(true); // Enable Double buffering

		canvas = new GLCanvas(capabilities);
		canvas.addGLEventListener(this);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addKeyListener(this);
		canvas.setAutoSwapBufferMode(true); // true by default. Just to be
											// explicit
		canvas.setFocusable(true);
		getContentPane().add(canvas);

		animator = new FPSAnimator(canvas, 60); // drive the display loop @ 60
												// FPS

		numTestCase = 5;
		testCaseID = 0;
		Nsteps = 12;
		centerOffset = new Vector3D();

		setTitle("CS480/680 PA4 by Renqing Gao");
		setSize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);

		rng = new Random();
		color = new ColorType(1.0f, 0.0f, 0.0f);
		lineSegs = new ArrayList<Point3D>();
		triangles = new ArrayList<Point3D>();
		doSmoothShading = false;
		doPhongShading = false;
	}

	public void run() {
		animator.start();
	}

	public static void main(String[] args) {
		PA4 P = new PA4();
		P.run();
	}

	public void initDepthBuffer() {
		int i, j;
		Dimension sz = this.getContentPane().getSize();
		for (i = 0; i < sz.getWidth(); i++) {
			for (j = 0; j < sz.getHeight(); j++) {
				this.depthBuffer[i][j] = -9999;
			}
		}
	}

	// ***********************************************
	// GLEventListener Interfaces
	// ***********************************************
	public void init(GLAutoDrawable drawable) {
	    System.out.println("==========================instructor=======================");
	    System.out.println("===1:ambient light switch=====|===T:switch test cases======");
	    System.out.println("===2:infinite light switch====|===8:turn on/off scale======");
	    System.out.println("===3:point light switch=======|===9:turn on/off rotate=====");
	    System.out.println("===4:spot light switch========|===0:turn on/off translate==");
	    System.out.println("===6:camera rotate switch=====|===7:camera translation=====");
	    System.out.println("===A:ambient term switch======|===up:x++/size up===========");
	    System.out.println("===S:specular term switch=====|===down:x--/size down=======");
	    System.out.println("===D:diffuse term switch======|===left:y++=================");
	    System.out.println("===M:switch material==========|===right:y--================");
	    System.out.println("===F:flat rendering===========|===ins:z++==================");
	    System.out.println("===G:gouraud rendering========|===del:z--==================");
	    System.out.println("===<:Nstep--==================|===>:Nstep++================");
	    System.out.println("===PgUp:camera rotate z=======|===PgDn:camera rotate z=====");
	    System.out.println("===P:phong rendering==========|===Q:exit===================");
	    System.out.println("=========================instructor========================");
		GL gl = drawable.getGL();
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		gl.glLineWidth(DEFAULT_LINE_WIDTH);
		Dimension sz = this.getContentPane().getSize();
		buff = new BufferedImage(sz.width, sz.height, BufferedImage.TYPE_3BYTE_BGR);
		depthBuffer = new float[sz.width][sz.height];
		clearPixelBuffer();
		this.initDepthBuffer();
	}

	// Redisplaying graphics
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		WritableRaster wr = buff.getRaster();
		DataBufferByte dbb = (DataBufferByte) wr.getDataBuffer();
		byte[] data = dbb.getData();

		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT, 1);
		gl.glDrawPixels(buff.getWidth(), buff.getHeight(), GL2.GL_BGR, GL2.GL_UNSIGNED_BYTE, ByteBuffer.wrap(data));
		drawTestCase();
	}

	// Window size change
	public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
		// deliberately left blank
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
		// deliberately left blank
	}

	void clearPixelBuffer() {
		lineSegs.clear();
		triangles.clear();
		Graphics2D g = buff.createGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, buff.getWidth(), buff.getHeight());
		g.dispose();
	}

	// drawTest
	void drawTestCase() {
		/* clear the window and vertex state */
		clearPixelBuffer();
		initDepthBuffer();
		// System.out.printf("Test case = %d\n",testCase);
		Random rand = new Random();
		float[] axis = new float[] { -1 , 1 , -1};

        // calculate appropriate quaternion
        final float rotate_delta = 3.1415927f / 360.0f;
        final float s = (float) Math.sin(0.5f * rotate_delta);
        final float c = (float) Math.cos(0.5f * rotate_delta);
        final Quaternion Q = new Quaternion(c, s * axis[0], s * axis[1], s * axis[2]);
        this.rotateQuaternion = Q.multiply(this.rotateQuaternion);
        
        // normalize to counteract acccumulating round-off error
        this.rotateQuaternion.normalize();

		//TODO add rotate quaternion
		testCases = new TestCases(
		        doSmoothShading , doPhongShading , Nsteps , ns , 
	            viewing_quaternion , viewing_center , 
	            buff , depthBuffer , materialOffset , 
	            centerOffset , rotateQuaternion , case4RotateSwitch , this.scaleParam);
		testCases.setLightSwitches(this.ambientSwitch, this.infiniteSwitch, this.pointSwitch, this.spotSwitch);
		testCases.setTermSwitches(this.ambientTermSwitch, this.specularTermSwitch, this.diffuseTermSwitch);
		switch (testCaseID) {
		case 0:
		    testCases.runTestCase0();
			break;
		case 1:
		    testCases.runTestCase1();
			break;
		case 2:
            testCases.runTestCase2();
            break;
		case 3:
            testCases.runTestCase3();
            break;
		case 4:
            testCases.runTestCase4();
            break;
		case 5:
            testCases.runTestCase5();
            break;
		case 6:
            testCases.runTestCase6();
            break;
		}
	}

	// ***********************************************
	// KeyListener Interfaces
	// ***********************************************
	public void keyTyped(KeyEvent key) {
		switch (key.getKeyChar()) {
		case 'Q':
		case 'q':
			new Thread() {
				public void run() {
					animator.stop();
				}
			}.start();
			System.exit(0);
			break;
		case 'C':
		case 'c':
			clearPixelBuffer();
			initDepthBuffer();
			break;
		case 'p':
		case 'P':
	        doPhongShading = !doPhongShading;
	        this.doSmoothShading = false;
		    break;
		case 'm':
		case 'M':
		    materialOffset = (materialOffset + 1) % materialNum;
		    break;
		case 'f':
		case 'F':
		    this.doSmoothShading = false;
		    this.doPhongShading = false;
		    break;
		case 'G':
		case 'g':
			doSmoothShading = !doSmoothShading; 
			this.doPhongShading = false;
			break;
		case 'T':
		case 't':
			testCaseID = (testCaseID + 1) % numTestCase;
			drawTestCase();
			break;
		case '<':
			Nsteps = Nsteps < 4 ? Nsteps : Nsteps / 2;
			drawTestCase();
			break;
		case '>':
			Nsteps = Nsteps > 190 ? Nsteps : Nsteps * 2;
			drawTestCase();
			break;
		case 'S':
        case 's':
            this.specularTermSwitch = !this.specularTermSwitch;
            drawTestCase();
            break;
        case 'D':
        case 'd':
            this.diffuseTermSwitch = !this.diffuseTermSwitch;
            drawTestCase();
            break;
        case 'A':
        case 'a':
            System.out.println(ambientTermSwitch + " amb switch");
            this.ambientTermSwitch = !this.ambientTermSwitch;
            drawTestCase();
            break;
		case '1':
        case '!':
            this.ambientSwitch = !this.ambientSwitch;
            drawTestCase();
            break;
        case '2':
        case '@':
            this.infiniteSwitch = !this.infiniteSwitch;
            drawTestCase();
            break;
        case '3':
        case '#':
            this.pointSwitch = !this.pointSwitch;
            drawTestCase();
            break;
        case '4':
        case '$':
            this.spotSwitch = !this.spotSwitch;
            drawTestCase();
            break;
        case '6':
        case '^':
            this.cameraRotateSwitch = !this.cameraRotateSwitch;
            this.cameraTranslateSwitch = false;
            this.scaleSwitch = false;
            this.translateSwitch = false;
            System.out.println("camera rotate switch:" + this.cameraRotateSwitch);
            drawTestCase();
            break;
        case '7':
        case '&':
            this.cameraTranslateSwitch = !this.cameraTranslateSwitch;
            this.cameraRotateSwitch = false;
            this.scaleSwitch = false;
            this.translateSwitch = false;
            System.out.println("camera translate switch:" + this.cameraTranslateSwitch);
            drawTestCase();
            break;
        case '8':
        case '*':
            this.scaleSwitch = !this.scaleSwitch;
            this.translateSwitch = false;
            this.cameraRotateSwitch = false;
            this.cameraTranslateSwitch = false;
            System.out.println("scale switch:" + this.scaleSwitch);
            drawTestCase();
            break;
		case '9':
        case '(':
            this.case4RotateSwitch = !this.case4RotateSwitch;
            System.out.println("Rotate switch:" + this.case4RotateSwitch);
            drawTestCase();
            break;
		case '0':
		case ')':
		    this.translateSwitch = !this.translateSwitch;
		    this.cameraRotateSwitch = false;
            this.cameraTranslateSwitch = false;
		    this.scaleSwitch = false;
		    System.out.println("translate switch:" + this.translateSwitch);
		    drawTestCase();
		    break;
		default:
			break;
		}
	}

	public void keyPressed(KeyEvent key) {
		switch (key.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			new Thread() {
				public void run() {
					animator.stop();
				}
			}.start();
			System.exit(0);
			break;
		case KeyEvent.VK_UP:
		    if(this.translateSwitch){
		        this.centerOffset.y -= 10f;
		    }
		    if(this.scaleSwitch){
		        this.scaleParam += 0.1;
		    }
		    if(this.cameraRotateSwitch){
		        Quaternion tempQ;
		        float sin, cos;
		        sin = (float) Math.sin(-3.1415927f / 180.0f);
		        cos = (float) Math.cos(-3.1415927f / 180.0f);
		        tempQ = new Quaternion(cos , sin * 1f , 0f , 0f);
		        this.viewing_quaternion = tempQ.multiply(viewing_quaternion);
		        this.viewing_quaternion.normalize();
		    }
		    if(this.cameraTranslateSwitch){
		        this.centerOffset.y += 10f;
		        viewing_center.y -= 10f;
		    }
		    break;
		case KeyEvent.VK_DOWN:
		    if(this.translateSwitch){
		        this.centerOffset.y += 10f;
		    }
		    if(this.scaleSwitch){
                this.scaleParam -= 0.1;
            }
		    if(this.cameraRotateSwitch){
		        Quaternion tempQ;
                float sin, cos;
                sin = (float) Math.sin(3.1415927f / 180.0f);
                cos = (float) Math.cos(3.1415927f / 180.0f);
                tempQ = new Quaternion(cos , sin * 1f , 0f , 0f);
                this.viewing_quaternion = tempQ.multiply(viewing_quaternion);
                this.viewing_quaternion.normalize();
            }
            if(this.cameraTranslateSwitch){
                this.centerOffset.y -= 10f;
                viewing_center.x += 10f;
            }
		    break;
		case KeyEvent.VK_LEFT:
		    if(this.translateSwitch)
		        this.centerOffset.x -= 10f;
	        if(this.cameraRotateSwitch){
	            Quaternion tempQ;
                float sin, cos;
                sin = (float) Math.sin(3.1415927f / 180.0f);
                cos = (float) Math.cos(3.1415927f / 180.0f);
                tempQ = new Quaternion(cos , 0f ,sin * 1f , 0f);
                this.viewing_quaternion = tempQ.multiply(viewing_quaternion);
                this.viewing_quaternion.normalize();
            }
            if(this.cameraTranslateSwitch){
                this.centerOffset.x += 10f;
                viewing_center.x -= 10f;
            }
		    break;
		case KeyEvent.VK_RIGHT:
		    if(this.translateSwitch)
		        this.centerOffset.x += 10f;
		    if(this.cameraRotateSwitch){
		        Quaternion tempQ;
                float sin, cos;
                sin = (float) Math.sin(-3.1415927f / 180.0f);
                cos = (float) Math.cos(-3.1415927f / 180.0f);
                tempQ = new Quaternion(cos , 0f ,sin * 1f , 0f);
                this.viewing_quaternion = tempQ.multiply(viewing_quaternion);
                this.viewing_quaternion.normalize();
            }
            if(this.cameraTranslateSwitch){
                this.centerOffset.x -= 10f;
                viewing_center.x += 10f;
            }
            break;
		case KeyEvent.VK_INSERT:
		    if(this.translateSwitch)
                this.centerOffset.z += 10f;
		    if(this.cameraTranslateSwitch){
                this.centerOffset.z -= 10f;
                viewing_center.z += 10f;
            }
		    break;
		case KeyEvent.VK_DELETE:
            if(this.translateSwitch)
                this.centerOffset.z -= 10f;
            if(this.cameraTranslateSwitch){
                this.centerOffset.z += 10f;
                viewing_center.z -= 10f;
            }
            break;
		case KeyEvent.VK_PAGE_UP:
		    if(this.cameraRotateSwitch){
                Quaternion tempQ;
                float sin, cos;
                sin = (float) Math.sin(-3.1415927f / 180.0f);
                cos = (float) Math.cos(-3.1415927f / 180.0f);
                tempQ = new Quaternion(cos , 0f , 0f , sin * 1f);
                this.viewing_quaternion = tempQ.multiply(viewing_quaternion);
                this.viewing_quaternion.normalize();
            }
		    break;
		case KeyEvent.VK_PAGE_DOWN:
		    if(this.cameraRotateSwitch){
                Quaternion tempQ;
                float sin, cos;
                sin = (float) Math.sin(3.1415927f / 180.0f);
                cos = (float) Math.cos(3.1415927f / 180.0f);
                tempQ = new Quaternion(cos , 0f , 0f , sin * 1f);
                this.viewing_quaternion = tempQ.multiply(viewing_quaternion);
                this.viewing_quaternion.normalize();
            }
		    break;
		default:
			break;
		}
	}

	public void keyReleased(KeyEvent key) {
		// deliberately left blank
	}

	// **************************************************
	// MouseListener and MouseMotionListener Interfaces
	// **************************************************
	public void mouseClicked(MouseEvent mouse) {
		// deliberately left blank
	}

	public void mousePressed(MouseEvent mouse) {
		int button = mouse.getButton();
		if (button == MouseEvent.BUTTON1) {
			last_x = mouse.getX();
			last_y = mouse.getY();
			rotate_world = true;
		}
	}

	public void mouseReleased(MouseEvent mouse) {
		int button = mouse.getButton();
		if (button == MouseEvent.BUTTON1) {
			rotate_world = false;
		}
	}

	public void mouseMoved(MouseEvent mouse) {
		// Deliberately left blank
	}

	/**
	 * Updates the rotation quaternion as the mouse is dragged.
	 * 
	 * @param mouse
	 *            The mouse drag event object.
	 */
	public void mouseDragged(final MouseEvent mouse) {
		if (this.rotate_world) {
			// get the current position of the mouse
			final int x = mouse.getX();
			final int y = mouse.getY();

			// get the change in position from the previous one
			final int dx = x - this.last_x;
			final int dy = y - this.last_y;

			// create a unit vector in the direction of the vector (dy, dx, 0)
			final float magnitude = (float) Math.sqrt(dx * dx + dy * dy);
			if (magnitude > 0.0001) {
				// define axis perpendicular to (dx,-dy,0)
				// use -y because origin is in upper lefthand corner of the
				// window
				final float[] axis = new float[] { -(float) (dy / magnitude), (float) (dx / magnitude), 0 };

				// calculate appropriate quaternion
				final float viewing_delta = 3.1415927f / 180.0f;
				final float s = (float) Math.sin(0.5f * viewing_delta);
				final float c = (float) Math.cos(0.5f * viewing_delta);
				final Quaternion Q = new Quaternion(c, s * axis[0], s * axis[1], s * axis[2]);
				this.viewing_quaternion = Q.multiply(this.viewing_quaternion);
				
				// normalize to counteract acccumulating round-off error
				this.viewing_quaternion.normalize();

				// save x, y as last x, y
				this.last_x = x;
				this.last_y = y;
				drawTestCase();
			}
		}

	}

	public void mouseEntered(MouseEvent mouse) {
		// Deliberately left blank
	}

	public void mouseExited(MouseEvent mouse) {
		// Deliberately left blank
	}

	public void dispose(GLAutoDrawable drawable) {

	}

	// **************************************************
	// Test Cases
	// Nov 9, 2014 Stan Sclaroff -- removed line and triangle test cases
	// **************************************************

	

}
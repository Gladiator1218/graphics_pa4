package Objects;
import Utils.Vector3D;

//****************************************************************************
//      Sphere class
//****************************************************************************
// History :
//   Nov 6, 2014 Created by Stan Sclaroff
//

public class SuperEllipsoid3D
{
	private Vector3D center;
	private float rx;
	private float ry;
	private float rz;
	private float s1 , s2;
	//m for cross section, n for sweeping
	private int m,n;
	public Mesh3D mesh;
	
	public SuperEllipsoid3D(Vector3D ellipCenter, float _rx, float _ry, float _rz, float _s1 , float _s2 , int _m, int _n)
	{
		center = new Vector3D(ellipCenter);
		rx = _rx;
		ry = _ry;
		rz = _rz;
		s1 = _s1;
		s2 = _s2;
		m = _m;
		n = _n;
		initMesh();
	}
	
	public float sign(float x){
	    if(x > 0){
	        return 1;
	    }
	    if(x < 0){
	        return -1;
	    }else{
	        return 0;
	    }
	}
	public float sign(int x){
	    if(x > 0){
            return 1;
        }
        if(x < 0){
            return -1;
        }else{
            return 0;
        }
    }
	public float sign(double x){
	    if(x > 0){
            return 1;
        }
        if(x < 0){
            return -1;
        }else{
            return 0;
        }
    }
	
	public void set_center(float _x, float _y, float _z)
	{
		center.x=_x;
		center.y=_y;
		center.z=_z;
		fillMesh();  // update the triangle mesh
	}
	
	public void set_radius(float _rx , float _ry , float _rz)
	{
		rx = _rx;
		ry = _ry;
		rz = _rz;
		fillMesh(); // update the triangle mesh
	}
	
	public void set_m(int _m)
	{
		m = _m;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public float get_s1(){
        return this.s1;
    }
    
    public void set_s1(float _s1){
        this.s1 = _s1;
        initMesh();
    }
    
    public float get_s2(){
        return this.s2;
    }
    
    public void set_s2(float _s2){
        this.s2 = _s2;
        initMesh();
    }
	
	public void set_n(int _n)
	{
		n = _n;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public int get_n()
	{
		return n;
	}
	
	public int get_m()
	{
		return m;
	}

	private void initMesh()
	{
		mesh = new Mesh3D(m,n);
		fillMesh();  // set the mesh vertices and normals
	}
		
	// fill the triangle mesh vertices and normals
	// using the current parameters for the sphere
	private void fillMesh()
	{
		int i,j;		
		float theta, phi;
		float d_theta=(float)(2.0*Math.PI)/ ((float)(m-1));
		float d_phi=(float)Math.PI / ((float)n-1);
		float c_theta,s_theta;
		float c_phi, s_phi;
		
		for(i=0,theta=-(float)Math.PI;i<m;++i,theta += d_theta)
	    {
			c_theta=(float)Math.cos(theta);
			s_theta=(float)Math.sin(theta);
			
			for(j=0,phi=(float)(-0.5*Math.PI);j<n;++j,phi += d_phi)
			{
				// vertex location
				c_phi = (float)Math.cos(phi);
				s_phi = (float)Math.sin(phi);

//                mesh.v[i][j].x=center.x+rx*c_phi*c_theta;
//                mesh.v[i][j].y=center.y+ry*c_phi*s_theta;
//                mesh.v[i][j].z=center.z+rz*s_phi;
				
				mesh.v[i][j].x=center.x + rx * sign(c_theta) * sign(c_phi) * (float)Math.pow(Math.abs(c_phi), s1) * (float)Math.pow(Math.abs(c_theta), s2);
				mesh.v[i][j].y=center.y + ry * sign(s_theta) * sign(c_phi) * (float)Math.pow(Math.abs(c_phi), s1) * (float)Math.pow(Math.abs(s_theta), s2);
				mesh.v[i][j].z=center.z + rz *  sign(s_phi) * (float)Math.pow(Math.abs(s_phi), s1);
				// unit normal to sphere at this vertex
				Vector3D du = new Vector3D();
				Vector3D dv = new Vector3D();
				
				du.x = rx * s1 * (float)Math.pow(Math.abs(c_phi), s1 - 1) * -s_phi * (float)Math.pow(Math.abs(c_theta), s2) * sign(c_theta);
                dv.x = rx * s2 *(float)Math.pow(Math.abs(c_theta), s2 - 1)* -s_theta * (float)Math.pow(Math.abs(c_phi), s1) * sign(c_phi);
                du.y = ry * s1 * (float)Math.pow(Math.abs(c_phi), s1 - 1) * -s_phi * (float)Math.pow(Math.abs(s_theta), s2) * sign(s_theta);
                dv.y = ry * s2 *(float)Math.pow(Math.abs(s_theta), s2 - 1)* c_theta * (float)Math.pow(Math.abs(c_phi), s1) * sign(c_phi);
				du.z = rz * s1 * (float)Math.pow(Math.abs(s_phi), s1 - 1) * c_phi;
                dv.z = 0;
                
                
                if(j == n - 1 || j == 0 )
                {
                    //du.crossProduct(dv, mesh.n[i][j]);
                    mesh.n[i][j].x = c_phi*c_theta;
                    mesh.n[i][j].y = c_phi*s_theta;
                    mesh.n[i][j].z= s_phi;
                }
                else
                {
                    dv.crossProduct(du, mesh.n[i][j]);
                }
				mesh.n[i][j].normalize();
//				System.out.printf("SuperEllipsoid:filling mesh[%d][%d] = (%.2f , %.2f , %.2f)\n" , i , j , mesh.n[i][j].x , mesh.n[i][j].y , mesh.n[i][j].z);
			}
	    }
	}
}
package Objects;
import Utils.Vector3D;

//****************************************************************************
//      Cylinder class originated from Sphere3D.java
//****************************************************************************
// History :
//   Nov 6, 2014 Created by Stan Sclaroff
//

public class Cylinder3D
{
	private Vector3D center;
	private float rx , rz , height;
	//m for cross section, n for sweeping
	private int m,n;
	public Mesh3D mesh;
	
	public Cylinder3D(float _x, float _y, float _z, float _rx , float _rz , float _height, int _m, int _n)
	{
		center = new Vector3D(_x,_y,_z);
		rx = _rx;
		rz = _rz;
		height = _height;
		m = _m;
		n = _n;
		initMesh();
	}
	
	public void set_radius(float _rx , float _rz){
	    rx = _rx;
	    rz = _rz;
	    initMesh();
	}
	
	public void set_height(float _height){
	    height = _height;
	    initMesh();
	}
	
	public void set_center(float _x, float _y, float _z)
	{
		center.x=_x;
		center.y=_y;
		center.z=_z;
		initMesh();  // update the triangle mesh
	}
	
	public void set_m(int _m)
	{
		m = _m;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public void set_n(int _n)
	{
		n = _n;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public int get_n()
	{
		return n;
	}
	
	public int get_m()
	{
		return m;
	}

	private void initMesh()
	{
		mesh = new Mesh3D(m,n);
		fillMesh();  // set the mesh vertices and normals
	}
		
	// fill the triangle mesh vertices and normals
	// using the current parameters for the sphere
	private void fillMesh()
	{
		int i,j;		
		float phi;
		float d_phi=(float)(2 * Math.PI) / ((float)n-1);
		float c_phi, s_phi;
		float tempY;
		float stepY = height / (float)(m - 1);
		Vector3D du = new Vector3D();
		Vector3D dv = new Vector3D();
		
		for(i = 0,tempY = -height / 2 ; i < m ; ++i , tempY += stepY)
	    {			
			for(j = 0 , phi = (float)(Math.PI) ; j < n ; ++j , phi += d_phi)
			{
				// vertex location
				c_phi = (float)Math.cos(phi);
				s_phi = (float)Math.sin(phi);
				mesh.v[i][j].x=center.x+rx * c_phi;
				mesh.v[i][j].y=center.y + tempY;
				mesh.v[i][j].z=center.z+rz * s_phi;
				
				// unit normal to sphere at this vertex
                du.x = -rx * s_phi;
                du.y = 0;
                du.z = rz * c_phi;
                dv.x = 0;
                dv.y = 1;
                dv.z = 0;
                
                mesh.n[i][j] = dv.crossProduct(du);
                mesh.n[i][j].normalize();
			}
	    }
	}
}
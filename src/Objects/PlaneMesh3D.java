package Objects;
import Utils.Quaternion;
import Utils.Vector3D;

//****************************************************************************
//       3D triangle mesh with normals
//****************************************************************************
// History :
//   Nov 6, 2014 Created by Stan Sclaroff
//   Nov 11, 2015 Modified by Renqing Gao to adapt mesh for a plane
//
public class PlaneMesh3D
{
	public int cols,rows;
	public Vector3D[][] v;
	public Vector3D[][] n;
	public Vector3D[] planeCenter;
	public Vector3D planeNormal;
	
	public PlaneMesh3D(int _rows, int _cols)
	{
		cols=_cols;
		rows=_rows;
		v = new Vector3D[rows][cols];
		n = new Vector3D[rows][cols];
		planeCenter = new Vector3D[rows];
		planeNormal = new Vector3D();
		
		for(int i=0;i<rows;++i)
			for(int j=0;j<cols;++j)
			{
				v[i][j] = new Vector3D();
				n[i][j] = new Vector3D();
			}
		for(int i =0; i < rows; i ++)
            planeCenter[i] = new Vector3D();
	}
	
	public void rotateMesh(Quaternion q, Vector3D center)
	{
		Quaternion q_inv = q.conjugate();
		Vector3D vec;
		
		Quaternion p;
		
		for(int i=0;i<rows;++i)
			for(int j=0;j<cols;++j)
			{
				// apply pivot rotation to vertices, given center point
				p = new Quaternion((float)0.0,v[i][j].minus(center)); 
				p=q.multiply(p);
				p=p.multiply(q_inv);
				vec = p.get_v();
				v[i][j]=vec.plus(center);
				
				// rotate the normals
				p = new Quaternion((float)0.0,n[i][j]);
				p=q.multiply(p);
				p=p.multiply(q_inv);
				n[i][j] = p.get_v();
			}
		//same thing above, calc planeCenter after rotate.
		for(int i = 0 ; i < rows ; i++){
		    p = new Quaternion((float) 0.0, planeCenter[i].minus(center));
		    p = q.multiply(p);
		    p = p.multiply(q_inv);
		    vec = p.get_v();
		    planeCenter[i] = vec.plus(center);
		}
		
	}
}
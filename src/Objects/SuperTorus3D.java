package Objects;
import Utils.Vector3D;

//****************************************************************************
//       Torus class
//****************************************************************************
// History :
//   Nov 9, 2014 Created by Stan Sclaroff
//

public class SuperTorus3D
{
	private Vector3D center;
	private float r,r_axial;
	private int m,n; // mesh dimensions
	public Mesh3D mesh;
	private float s1 , s2;
	
	public SuperTorus3D(Vector3D center, float _r, float _r_axial, float _s1 , float _s2 , int _m, int _n)
	{
		this.center = new Vector3D(center);
		r = _r;
		r_axial = _r_axial;
		m = _m;
		n = _n;
		s1 = _s1;
		s2 = _s2;
		initMesh();
	}
	
	public float sign(float x){
        if(x > 0){
            return 1;
        }
        if(x < 0){
            return -1;
        }else{
            return 0;
        }
	}
    public float sign(int x){
        if(x > 0){
            return 1;
        }
        if(x < 0){
            return -1;
        }else{
            return 0;
        }
    }
    public float sign(double x){
        if(x > 0){
            return 1;
        }
        if(x < 0){
            return -1;
        }else{
            return 0;
        }
    }
	
	public void set_center(float _x, float _y, float _z)
	{
		center.x=_x;
		center.y=_y;
		center.z=_z;
		fillMesh(); 		// update the triangle mesh
	}
	
    public float get_s1(){
        return this.s1;
    }
    
    public void set_s1(float _s1){
        this.s1 = _s1;
        initMesh();
    }
    
    public float get_s2(){
        return this.s2;
    }
    
    public void set_s2(float _s2){
        this.s2 = _s2;
        initMesh();
    }
	
	public void set_radius(float _r)
	{
		r = _r;
		fillMesh();		// update the triangle mesh
	}
	
	public void set_m(int _m)
	{
		m = _m;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public void set_n(int _n)
	{
		n = _n;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public int get_n()
	{
		return n;
	}
	
	public int get_m()
	{
		return m;
	}

	private void initMesh()
	{
		mesh = new Mesh3D(m,n);
		fillMesh();
	}
		
	// given the current parameters for the torus
	// fill the triangle mesh with vertices and normals
	private void fillMesh()
	{
		int i,j;		
		float theta, phi;
		float d_theta=(float)(2.0*Math.PI)/ ((float)m-1);
		float d_phi=(float)(2.0*Math.PI) / ((float)n-1);
		float c_theta,s_theta;
		float c_phi, s_phi;
		Vector3D du = new Vector3D();
		Vector3D dv = new Vector3D();
		
		for(i=0,theta=(float)-Math.PI;i<m;++i,theta += d_theta)
	    {
			c_theta=(float)Math.cos(theta);
			s_theta=(float)Math.sin(theta);
			
			for(j=0,phi=(float)-Math.PI;j<n;++j,phi += d_phi)
			{
				// follow the formulation for torus given in textbook
				c_phi = (float)Math.cos(phi);
				s_phi = (float)Math.sin(phi);
				mesh.v[i][j].x=(float) (center.x + (r_axial + r * sign(c_phi) * Math.pow(Math.abs(c_phi) , s1)) * sign(c_theta) * Math.pow(Math.abs(c_theta) , s2));
				mesh.v[i][j].y=(float) (center.y + (r_axial + r * sign(c_phi) * Math.pow(Math.abs(c_phi) , s1)) * sign(s_theta) * Math.pow(Math.abs(s_theta) , s2));
				mesh.v[i][j].z=(float) (center.z + r * sign(s_phi) * Math.pow(Math.abs(s_phi) , this.s1));
				
				// compute partial derivatives
				// then use cross-product to get the normal
				// and normalize to produce a unit vector for the normal
				//d theta
				du.x = (float)( (r_axial + r * sign(c_phi) * Math.pow(Math.abs(c_phi) , s1)) * -s_theta * s2 * Math.pow(Math.abs(c_theta) , s2 - 1) );
				du.y = (float)( (r_axial + r * sign(c_phi) * Math.pow(Math.abs(c_phi) , s1)) * c_theta * s2 * Math.pow(Math.abs(s_theta) , s2 - 1) );
				du.z = 0;
				
				//d phi
				dv.x = (float)( (r * -s_phi * Math.pow(Math.abs(c_phi) , s1 - 1)) * sign(c_theta) * Math.pow(Math.abs(c_theta) , s2) );
                dv.y = (float)( (r * -s_phi * Math.pow(Math.abs(c_phi) , s1 - 1)) * sign(s_theta) * Math.pow(Math.abs(s_theta) , s2) );
                dv.z = (float)( r * c_phi * s1 * Math.pow(Math.abs(s_phi) , s1 - 1) );
                
				du.crossProduct(dv, mesh.n[i][j]);
				mesh.n[i][j].normalize();
			}
	    }
	}
}
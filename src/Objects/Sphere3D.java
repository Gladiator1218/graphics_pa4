package Objects;
import java.awt.image.BufferedImage;

import Utils.Vector3D;

//****************************************************************************
//      Sphere class
//****************************************************************************
// History :
//   Nov 6, 2014 Created by Stan Sclaroff
//

public class Sphere3D
{
	private Vector3D center;
	private float r;
	//m for cross section, n for sweeping
	private int m,n;
	public Mesh3D mesh;
	public boolean bump = false;
	public BufferedImage bumpMap;
	
	public Sphere3D(float _x, float _y, float _z, float _r, int _m, int _n)
	{
		center = new Vector3D(_x,_y,_z);
		r = _r;
		m = _m;
		n = _n;
		initMesh();
	}
	
	public void setBump(boolean _bump , BufferedImage _bumpMap){
	    this.bump = _bump;
	    if(_bump)
	        this.bumpMap = _bumpMap;
	    this.initMesh();
	}
	
	public void set_center(float _x, float _y, float _z)
	{
		center.x=_x;
		center.y=_y;
		center.z=_z;
		fillMesh();  // update the triangle mesh
	}
	
	public void set_radius(float _r)
	{
		r = _r;
		fillMesh(); // update the triangle mesh
	}
	
	public void set_m(int _m)
	{
		m = _m;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public void set_n(int _n)
	{
		n = _n;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public int get_n()
	{
		return n;
	}
	
	public int get_m()
	{
		return m;
	}

	private void initMesh()
	{
		mesh = new Mesh3D(m,n);
		fillMesh();  // set the mesh vertices and normals
	}
	
    public float calc_bu(BufferedImage bumpMap, Vector3D n)
    {
        int mapWidth = bumpMap.getWidth() - 1;
        int mapHeight = bumpMap.getHeight() - 1;
        float textureCoordU = (float)(0.8 * Math.asin(n.x)/ Math.PI) + 0.6f;
        float textureCoordV = (float)(0.8 * Math.asin(n.y)/ Math.PI) + 0.6f;
        float temp1 = bumpMap.getRGB((int)(textureCoordU * mapWidth)+1, (int)(textureCoordV * mapHeight)) & 0xff;
        float temp2 = bumpMap.getRGB((int)(textureCoordU * mapWidth), (int)(textureCoordV * mapHeight)) & 0xff;
        float res = temp1 - temp2;
        return res;
    }
    
    public float calc_bv(BufferedImage bumpMap,Vector3D n)
    {
        int mapWidth = bumpMap.getWidth() - 1;
        int mapHeight = bumpMap.getHeight() - 1;
        float textureCoordU = (float)(0.8 * Math.asin(n.x)/ Math.PI) + 0.6f;
        float textureCoordV = (float)(0.8 * Math.asin(n.y)/ Math.PI) + 0.6f;
        float temp1 = bumpMap.getRGB((int)(textureCoordU * mapWidth), (int)(textureCoordV * mapHeight)+1) & 0xff;
        float temp2 = bumpMap.getRGB((int)(textureCoordU * mapWidth), (int)(textureCoordV * mapHeight)) & 0xff;
        float res = temp1 - temp2;
        return res; 
    }
	
	// fill the triangle mesh vertices and normals
	// using the current parameters for the sphere
	private void fillMesh()
	{
		int i,j;		
		float theta, phi;
		float d_theta=(float)(2.0*Math.PI)/ ((float)(m-1));
		float d_phi=(float)Math.PI / ((float)n-1);
		float c_theta,s_theta;
		float c_phi, s_phi;
		
		for(i=0,theta=-(float)Math.PI;i<m;++i,theta += d_theta)
	    {
			c_theta=(float)Math.cos(theta);
			s_theta=(float)Math.sin(theta);
			
			for(j=0,phi=(float)(-0.5*Math.PI);j<n;++j,phi += d_phi)
			{
				// vertex location
				c_phi = (float)Math.cos(phi);
				s_phi = (float)Math.sin(phi);
				mesh.v[i][j].x=center.x+r*c_phi*c_theta;
				mesh.v[i][j].y=center.y+r*c_phi*s_theta;
				mesh.v[i][j].z=center.z+r*s_phi;
				
				// unit normal to sphere at this vertex
				mesh.n[i][j].x = c_phi*c_theta;
				mesh.n[i][j].y = c_phi*s_theta;
				mesh.n[i][j].z = s_phi;

		        //according to class note: wk8-cls1-.BEmap.pdf
				if(this.bump)
                {
				    //d theta
                    Vector3D pu = new Vector3D(-r * c_phi * s_theta , r * c_phi * c_theta ,  0);
				    //d phi
				    Vector3D pv = new Vector3D(-r * s_phi * c_theta ,-r * s_phi * s_theta  , r * c_phi); 
				    
                    Vector3D N = new Vector3D();
                    //calc N
                    pu.crossProduct(pv, N);
                    
                    //calc Bu and Bv using the bumpMap
                    float bu = calc_bu(this.bumpMap , mesh.n[i][j]);
                    float bv = calc_bv(this.bumpMap , mesh.n[i][j]);
                    
                    Vector3D term1 = new Vector3D();
                    Vector3D term2 = new Vector3D();
                    pu.crossProduct(mesh.n[i][j],term1);
                    mesh.n[i][j].crossProduct(pv,term2);
                    
                    term1 = term1.scale(bv);
                    term2 = term2.scale(bu);
                    
                    mesh.n[i][j] = N.plus(term1.plus(term2));
                    mesh.n[i][j].normalize();
                }
			}
	    }
	}
}
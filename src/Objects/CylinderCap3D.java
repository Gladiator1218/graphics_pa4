package Objects;
import Utils.Vector3D;

//****************************************************************************
//      Cylinder class originated from Sphere3D.java
//****************************************************************************
// History :
//   Nov 6, 2014 Created by Stan Sclaroff
//

public class CylinderCap3D
{
	private Vector3D center;
	private float rx , rz;
	//m for cross section, n for sweeping
	private int m,n;
	public boolean topCap;
	public PlaneMesh3D mesh;
	
	public CylinderCap3D(Vector3D cylinderCapCenter, float _rx , float _rz , int _m, int _n , boolean _topCap)
	{
		center = cylinderCapCenter;
		rx = _rx;
		rz = _rz;
		m = _m;
		if(m != 1){
		    m = 1;
		    System.out.println("WARNING CylinderCap3D: m been set to 1");
		}
		n = _n;
		topCap = _topCap;
		initMesh();
	}
	
	public void set_radius(float _rx , float _rz){
	    rx = _rx;
	    rz = _rz;
	    initMesh();
	}
	
	public void set_center(float _x, float _y, float _z)
	{
		center.x=_x;
		center.y=_y;
		center.z=_z;
		initMesh();  // update the triangle mesh
	}
	
	public void set_m(int _m)
	{
		m = _m;
		if(m != 1){
            m = 1;
            System.out.println("WARNING CylinderCap3D: m been set to 1");
        }
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public void set_n(int _n)
	{
		n = _n;
		initMesh(); // resized the mesh, must re-initialize
	}
	
	public int get_n()
	{
		return n;
	}
	
	public int get_m()
	{
		return m;
	}

	private void initMesh()
	{
		mesh = new PlaneMesh3D(m,n);
		for(int i = 0 ; i < m ; i++)
		    fillMesh(i);  // set the mesh vertices and normals
	}
		
	// fill the triangle mesh vertices and normals
	// using the current parameters for the sphere
	private void fillMesh(int index)
	{		
	    int j;
		float phi;
		float d_phi=(float)(2 * Math.PI) / ((float)n-1);
		float c_phi, s_phi;
		this.mesh.planeCenter[index] = center;
		if(this.topCap){
		    this.mesh.planeNormal = new Vector3D(0 , 1 , 0);
		    for(j = 0 , phi = (float)(Math.PI) ; j < n ; ++j , phi += d_phi)
	        {
	            // vertex location
	            c_phi = (float)Math.cos(phi);
	            s_phi = (float)Math.sin(phi);
	            mesh.v[index][j].x=center.x + rx * c_phi;
	            mesh.v[index][j].y=center.y;
	            mesh.v[index][j].z=center.z + rz * s_phi;
	            mesh.n[index][j] = new Vector3D(0 , 1f , 0);

	        }
		}
        else{
            this.mesh.planeNormal = new Vector3D(0 , -1 , 0);
            for(j = 0 , phi = -(float)Math.PI ; j < n ; ++j , phi -= d_phi)
            {
                // vertex location
                c_phi = (float)Math.cos(phi);
                s_phi = (float)Math.sin(phi);
                mesh.v[index][j].x=center.x + rx * c_phi;
                mesh.v[index][j].y=center.y;
                mesh.v[index][j].z=center.z + rz * s_phi;
                mesh.n[index][j] = new Vector3D(0 , -1f , 0);
            }
        }		
    }
	
}
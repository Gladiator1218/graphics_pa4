package Objects;
import Utils.Vector3D;

//****************************************************************************
//       Box class originated from Torus.java
//****************************************************************************
// History :
//   Nov 9, 2014 Created by Stan Sclaroff
//

public class Box3D
{
	private Vector3D center;
	public float length;
    public float width;
    public float height;
	private int m,n; // mesh dimensions
	public PlaneMesh3D mesh;
	private boolean testNET = false;
	
	//height for y axis
	//width for x axis
	//length for z axis
	public Box3D(Vector3D boxCenter, float _length, float _width, float _height, int _m, int _n)
	{
		center = new Vector3D(boxCenter.x , boxCenter.y , boxCenter.z);
		height = _height;
		length = _length;
		width = _width;
		m = _m;
		if(m != 6){
		    System.out.printf("\nWARNING Box3D init: m has to be 6 for box(6 faces)\n");
		    m = 6;
		}
		n = _n;
		if(testNET){
		    n = 2;
		}
		initMesh();
	}
	
	

	private void initMesh()
	{
	    //for one plane(m, 6 planes actually), we got 4 edges and n(like higher solution when n grows? looks like n could just be 2) points on every edge.
		mesh = new PlaneMesh3D(m , 4 * n);
		//gotta fill 6 feces
		for(int i = 0 ; i < m ; i++){
		    fillMesh(i);
		    switch(i){
		    case 0:   //near
                mesh.planeCenter[i] = new Vector3D(center.x , center.y , center.z + length / 2);
                break;
		    case 1:   //far
                mesh.planeCenter[i] = new Vector3D(center.x , center.y , center.z - length / 2);
                break;
		    case 2:   //left
                mesh.planeCenter[i] = new Vector3D(center.x + width / 2, center.y , center.z);
                break;
		    case 3:   //right
                mesh.planeCenter[i] = new Vector3D(center.x - width / 2, center.y , center.z);
                break;
		    case 4:   //top
                mesh.planeCenter[i] = new Vector3D(center.x , center.y + height / 2, center.z);
                break;
		    case 5:   //bottom
                mesh.planeCenter[i] = new Vector3D(center.x , center.y - height / 2 , center.z);
                break;
		    default:
		        System.out.printf("ERROR Box3D initMesh: index not in range:[0 , 5]\n");
		    }
		}
		
	}
		
	// given the current parameters for the torus
	// fill the triangle mesh with vertices and normals
	private void fillMesh(int index)
	{
		float stepX = (float)width / (float)(n - 1);
		float stepY = (float)height / (float)(n - 1);
		float stepZ = (float)length / (float)(n - 1);
		float tempX = 0f;
		float tempY = 0f;
		float tempZ = 0f;
		
		//fill four edges in one case
		switch(index){
		case 0://near
		    tempZ = length / 2;
		    tempY = height / 2;
		    tempX = -width / 2;
		    for(int i = 0 ; i < n ; i++){
		        fillMeshPoint(index , i , tempX , tempY , tempZ);
    		    mesh.n[index][i].x = 0;
    		    mesh.n[index][i].y = 0;
    		    mesh.n[index][i].z = 1f;
    		    fillMeshPoint(index , i + 2 * n , tempX , -1 * tempY , tempZ);
                mesh.n[index][i + 2 * n].x = 0;
                mesh.n[index][i + 2 * n].y = 0;
                mesh.n[index][i + 2 * n].z = 1f;
                tempX = tempX + stepX;
		    }
		    tempZ = length / 2;
            tempX = width / 2;
            tempY = -height / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i + n , tempX , tempY , tempZ);
                mesh.n[index][i + n].x = 0;
                mesh.n[index][i + n].y = 0;
                mesh.n[index][i + n].z = 1f;
                fillMeshPoint(index , i + 3 * n , -1 * tempX , tempY , tempZ);
                mesh.n[index][i + 3 * n].x = 0;
                mesh.n[index][i + 3 * n].y = 0;
                mesh.n[index][i + 3 * n].z = 1f;
                tempY = tempY + stepY;
            }
		    break;
		case 1://far
		    tempZ = -length / 2;
            tempY = height / 2;
            tempX = -width / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i , tempX , tempY , tempZ);
                mesh.n[index][i].x = 0;
                mesh.n[index][i].y = 0;
                mesh.n[index][i].z = -1f;
                fillMeshPoint(index , i + 2 * n , tempX , -1 * tempY , tempZ);
                mesh.n[index][i + 2 * n].x = 0;
                mesh.n[index][i + 2 * n].y = 0;
                mesh.n[index][i + 2 * n].z = -1f;
                tempX += stepX;
            }
            tempZ = -length / 2;
            tempX = width / 2;
            tempY = -height / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i + n , tempX , tempY , tempZ);
                mesh.n[index][i + n].x = 0;
                mesh.n[index][i + n].y = 0;
                mesh.n[index][i + n].z = -1f;
                fillMeshPoint(index , i + 3 * n , -1 * tempX , tempY , tempZ);
                mesh.n[index][i + 3 * n].x = 0;
                mesh.n[index][i + 3 * n].y = 0;
                mesh.n[index][i + 3 * n].z = -1f;
                tempY += stepY;
            }
            break;
		case 2://left
		    tempX = -width / 2;
		    tempZ = length / 2;
            tempY = -height / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i , tempX , tempY , tempZ);
                mesh.n[index][i].x = -1f;
                mesh.n[index][i].y = 0;
                mesh.n[index][i].z = 0;
                fillMeshPoint(index , i + 2 * n , tempX , tempY , -1 * tempZ);
                mesh.n[index][i + 2 * n].x = -1f;
                mesh.n[index][i + 2 * n].y = 0;
                mesh.n[index][i + 2 * n].z = 0;
                tempY += stepY;
            }
            tempX = -width / 2;
            tempY = height / 2;
            tempZ = -length / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i + n , tempX , tempY , tempZ);
                mesh.n[index][i + n].x = -1f;
                mesh.n[index][i + n].y = 0;
                mesh.n[index][i + n].z = 0;
                fillMeshPoint(index , i + 3 * n , tempX , -1 * tempY , tempZ);
                mesh.n[index][i + 3 * n].x = -1f;
                mesh.n[index][i + 3 * n].y = 0;
                mesh.n[index][i + 3 * n].z = 0;
                tempZ += stepZ;
            }
            break;
		case 3://right
            tempX = width / 2;
            tempZ = length / 2;
            tempY = -height / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i , tempX , tempY , tempZ);
                mesh.n[index][i].x = 1f;
                mesh.n[index][i].y = 0;
                mesh.n[index][i].z = 0;
                fillMeshPoint(index , i +  2 * n , tempX , tempY , -1 * tempZ);
                mesh.n[index][i + 2 * n].x = 1f;
                mesh.n[index][i + 2 * n].y = 0;
                mesh.n[index][i + 2 * n].z = 0;
                tempY += stepY;
            }
            tempX = width / 2;
            tempY = height / 2;
            tempZ = -length / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i + n , tempX , tempY , tempZ);
                mesh.n[index][i + n].x = 1f;
                mesh.n[index][i + n].y = 0;
                mesh.n[index][i + n].z = 0;
                fillMeshPoint(index , i + 3 * n , tempX , -1 * tempY , tempZ);
                mesh.n[index][i + 3 * n].x = 1f;
                mesh.n[index][i + 3 * n].y = 0;
                mesh.n[index][i + 3 * n].z = 0;
                tempZ += stepZ;
            }
            break;
		case 4://top
		    tempY = height / 2;
		    tempX = width / 2;
		    tempZ = -length / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i , tempX , tempY , tempZ);
                mesh.n[index][i].x = 0;
                mesh.n[index][i].y = 1f;
                mesh.n[index][i].z = 0;
                fillMeshPoint(index , i + 2 * n , -1 * tempX , tempY , tempZ);
                mesh.n[index][i + 2 * n].x = 0;
                mesh.n[index][i + 2 * n].y = 1f;
                mesh.n[index][i + 2 * n].z = 0;
                tempZ += stepZ;
            }
            tempY = height / 2;
            tempZ = length / 2;
            tempX = -width / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i + n , tempX , tempY , tempZ);
                mesh.n[index][i + 1 * n].x = 0;
                mesh.n[index][i + 1 * n].y = 1f;
                mesh.n[index][i + 1 * n].z = 0;
                fillMeshPoint(index , i + 3 * n , tempX , tempY , -1 * tempZ);
                mesh.n[index][i + 3 * n].x = 0;
                mesh.n[index][i + 3 * n].y = 1f;
                mesh.n[index][i + 3 * n].z = 0;
                tempX += stepX;
            }
            break;
		case 5://bottom
            tempY = -height / 2;
            tempX = width / 2;
            tempZ = -length / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i , tempX , tempY , tempZ);
                mesh.n[index][i].x = 0;
                mesh.n[index][i].y = -1f;
                mesh.n[index][i].z = 0;
                fillMeshPoint(index , i  + 2 * n , -1 * tempX , tempY , tempZ);
                mesh.n[index][i + 2 * n].x = 0;
                mesh.n[index][i + 2 * n].y = -1f;
                mesh.n[index][i + 2 * n].z = 0;
                tempZ += stepZ;
            }
            tempY = -height / 2;
            tempZ = length / 2;
            tempX = -width / 2;
            for(int i = 0 ; i < n ; i++){
                fillMeshPoint(index , i + n , tempX , tempY , tempZ);
                mesh.n[index][i + 1 * n].x = 0;
                mesh.n[index][i + 1 * n].y = -1f;
                mesh.n[index][i + 1 * n].z = 0;
                fillMeshPoint(index , i + 3 * n, tempX , tempY , -1 * tempZ);
                mesh.n[index][i + 3 * n].x = 0;
                mesh.n[index][i + 3 * n].y = -1f;
                mesh.n[index][i + 3 * n].z = 0;
                tempX += stepX;
            }
            break;
		}
	}
	
	public void fillMeshPoint(int index, int n, float x , float y , float z){
//	    System.out.printf("v[%d][%d] = (%.3f , %.3f , %.3f)\n" , index , n , x , y , z);
	    this.mesh.v[index][n].x = center.x + x;
        this.mesh.v[index][n].y = center.y + y;
        this.mesh.v[index][n].z = center.z + z;
	}
	
	public void set_center(float _x, float _y, float _z)
    {
        center.x=_x;
        center.y=_y;
        center.z=_z;
        initMesh();         // update the triangle mesh
    }
    
    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
        initMesh();
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
        initMesh();
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
        initMesh();
    }
    
    public void set_m(int _m)
    {
        m = _m;
        initMesh(); // resized the mesh, must re-initialize
    }
    
    public void set_n(int _n)
    {
        n = _n;
        initMesh(); // resized the mesh, must re-initialize
    }
    
    public int get_n()
    {
        return n;
    }
    
    public int get_m()
    {
        return m;
    }
}
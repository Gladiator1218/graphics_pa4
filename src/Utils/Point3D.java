package Utils;

//****************************************************************************
//       DD Point Class from Point2D
//****************************************************************************
// History :
//   Nov 6, 2014 Created by Stan Sclaroff
//	 Nov 18, 2015 Updated by Renqing Gao
//

public class Point3D
{
	public int x, y; 
	public float z;
	public float u, v; // uv coordinates for texture mapping
	public Vector3D n;
	public ColorType c;
	public Point3D(int _x, int _y, int _z, ColorType _c)
	{
		u = 0;
		v = 0;
		x = _x;
		y = _y;
		z = _z;
		c = _c;
		n = new Vector3D();
	}
	public Point3D(int _x, int _y, int _z, ColorType _c, float _u, float _v)
	{
		u = _u;
		v = _v;
		x = _x;
		y = _y;
		y = _z;
		c = _c;
	}
	public Point3D()
	{
		c = new ColorType(1.0f, 1.0f, 1.0f);
	}
	public Point3D( Point3D p)
	{
		u = p.u;
		v = p.v;
		x = p.x;
		y = p.y;
		z = p.z;
		if(p.n != null)
		    n = new Vector3D(p.n);
		else{
		    n = new Vector3D();
		}
		c = new ColorType(p.c.r, p.c.g, p.c.b);
	}
}
package Utils;
//****************************************************************************
//       material class
//****************************************************************************
// History :
//   Nov 6, 2014 Created by Stan Sclaroff
//
public class Material 
{
	public ColorType ka, kd, ks;
	public int ns;
	public boolean specular, diffuse, ambient, radial, angular;
	
	//reference from http://paulbourke.net/dataformats/mtl/
	public static final Material Mat1 = new Material(new ColorType(0 , 1 , 0) , new ColorType(0 , 1 , 0) , new ColorType(1 , 1 , 1) , 9 , true , true);
	public static final Material Mat2 = new Material(new ColorType(0.2f , 0.2f , 0.2f) , new ColorType(0.6f , 0.6f , 0.6f) , new ColorType(0.1f , 0.1f , 0.1f) , 200 , true , true);
	public static final Material Mat3 = new Material(new ColorType(1 , 0 , 0) , new ColorType(1 , 0 , 0) , new ColorType(1 , 1 , 1) , 3 , true , true);
	public static final Material Mat4 = new Material(new ColorType(1 , 1 , 0) , new ColorType(1 , 1 , 0) , new ColorType(1 , 1 , 1) , 6 , true , true);
	public static final Material Mat5 = new Material(new ColorType(1 , 0 , 1) , new ColorType(1 , 0 , 1) , new ColorType(0.8f , 0 , 0.8f) , 5 , true , true);
	public static final Material Mat6 = new Material(new ColorType(1 , 0.5f , 1) , new ColorType(1 , 0.5f , 1) , new ColorType(0.8f , 1f , 0.8f) , 1 , true , false);
	public static final Material Mat7 = new Material(new ColorType(1 , 0 , 0.5f) , new ColorType(1 , 0 , 0.5f) , new ColorType(0.8f , 1f , 0.8f) , 50 , false , true);
	public static final Material Mat8 = new Material(new ColorType(0.5f , 0 , 1) , new ColorType(0.5f , 0 , 1) , new ColorType(0.8f , 1f , 0.8f) , 25 , false , false);
	
	public Material(ColorType _ka, ColorType _kd, ColorType _ks, int _ns , boolean radial , boolean angular)
	{
		ks = new ColorType(_ks);  // specular coefficient for r,g,b
		ka = new ColorType(_ka);  // ambient coefficient for r,g,b
		kd = new ColorType(_kd);  // diffuse coefficient for r,g,b
		ns = _ns;  // specular exponent
		
		// set boolean variables 
		specular = (ns>0 && (ks.r > 0.0 || ks.g > 0.0 || ks.b > 0.0));
		diffuse = (kd.r > 0.0 || kd.g > 0.0 || kd.b > 0.0);
		ambient = (ka.r > 0.0 || ka.g > 0.0 || ka.b > 0.0);
		this.radial = true;
		this.angular = true;
	}
}
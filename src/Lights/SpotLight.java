package Lights;
import Utils.ColorType;
import Utils.Material;
import Utils.Vector3D;

//****************************************************************************
//       Point light source class
//****************************************************************************
// History :
//   Nov 29, 2015 Created by Renqing Gao, Based on InfiniteLight.java
//
public class SpotLight 
{
    public Vector3D position;
	public Vector3D direction;
	public ColorType color;
	public int theta;//Angular attenuation
	public float radialAttenuation[] = { 0.1f, 0.1f, 0.1f }; 
	public int al = 5; 
	public boolean ambientTermSwitch = true;
    public boolean specularTermSwitch = true;
    public boolean diffuseTermSwitch = true;
	
	public SpotLight(ColorType _c, Vector3D _position, Vector3D _direction, int _theta, int _al , boolean a , boolean s , boolean d)
	{
		color = new ColorType(_c);
		position = new Vector3D(_position);
		direction = new Vector3D(_direction);
		theta = _theta;
		al = _al;
		this.direction.normalize();
		ambientTermSwitch = a;
		specularTermSwitch = s;
		diffuseTermSwitch = d;
	}
	
	public double calcRadialAttenuation(double distance)
    {
        double attenuation = 0;
        attenuation = 1.0 / (radialAttenuation[0] + radialAttenuation[1] * distance + radialAttenuation[2] * Math.pow(distance, 2));
        return attenuation;
    }
	
	// apply this light source to the vertex / normal, given material
	// return resulting color value
	public ColorType applyLight(Material mat, Vector3D v, Vector3D n, Vector3D objPosition){
		ColorType res = new ColorType();
		Vector3D obj_vector = this.position.minus(objPosition); 
		obj_vector.normalize();
		float cos_alpha = obj_vector.dotProduct(this.direction);
		if(cos_alpha < Math.cos(this.theta)){
		    return res;
		}
		this.direction = this.position.minus(objPosition);
		this.direction.normalize();
		// dot product between light direction and normal
		// light must be facing in the positive direction
		// dot <= 0.0 implies this light is facing away (not toward) this point
		// therefore, light only contributes if dot > 0.0
		double dot = direction.dotProduct(n);
		if(dot>0.0)
		{
			// diffuse component
			if(mat.diffuse && this.diffuseTermSwitch)
			{
				res.r = (float)(dot*mat.kd.r*color.r);
				res.g = (float)(dot*mat.kd.g*color.g);
				res.b = (float)(dot*mat.kd.b*color.b);
			}
			// specular component
			if(mat.specular && this.specularTermSwitch)
			{
				Vector3D r = direction.reflect(n);
				dot = r.dotProduct(v);
				if(dot>0.0)
				{
					res.r += (float)Math.pow(dot , mat.ns) * mat.ks.r * color.r;
					res.g += (float)Math.pow(dot , mat.ns) * mat.ks.g * color.g;
					res.b += (float)Math.pow(dot , mat.ns) * mat.ks.b * color.b;
				}
			}
            if(mat.radial)
            {
                float distance;
                /*radial attenuation*/
                distance = objPosition.distance(this.position);
                distance = distance / 100;
                float radialAttenuation = (float)calcRadialAttenuation(distance);
                res.r = res.r * radialAttenuation;
                res.g = res.g * radialAttenuation;
                res.b = res.b * radialAttenuation;
            }
            if(mat.angular)
            {
                /*angular attenuation*/
                float angularAttenuation = (float) Math.pow(cos_alpha, this.al);
                res.r = res.r * angularAttenuation;
                res.g = res.g * angularAttenuation;
                res.b = res.b * angularAttenuation;
            }
			
			// clamp so that allowable maximum illumination level is not exceeded
			res.r = (float) Math.min(1.0, res.r);
			res.g = (float) Math.min(1.0, res.g);
			res.b = (float) Math.min(1.0, res.b);
		}
		return(res);
	}
}

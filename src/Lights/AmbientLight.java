package Lights;
import Utils.ColorType;
import Utils.Material;
import Utils.Vector3D;

//****************************************************************************
//       Ambient light source class
//****************************************************************************
// History :
//   Nov 29, 2015 Created by Renqing Gao, Based on InfiniteLight.java
//
public class AmbientLight 
{
    public Vector3D position;
	public Vector3D direction;
	public ColorType color;
	public boolean ambientTermSwitch = true;
	public boolean specularTermSwitch = true;
	public boolean diffuseTermSwitch = true;
	
	public AmbientLight(ColorType _c, boolean a , boolean s , boolean d)
	{
		color = new ColorType(_c);
		ambientTermSwitch = a;
        specularTermSwitch = s;
        diffuseTermSwitch = d;
	}
	
	// apply this light source to the vertex / normal, given material
	// return resulting color value
	public ColorType applyLight(Material mat, Vector3D v, Vector3D n, Vector3D objPosition){
		ColorType res = new ColorType();
		if(mat.ambient && this.ambientTermSwitch)
		{
			res.r = (float)mat.ka.r * color.r;
			res.g = (float)mat.ka.g * color.g;
			res.b = (float)mat.ka.b * color.b;
		}
		
		// clamp so that allowable maximum illumination level is not exceeded
		res.r = (float) Math.min(1.0, res.r);
		res.g = (float) Math.min(1.0, res.g);
		res.b = (float) Math.min(1.0, res.b);
		return(res);
	}
}

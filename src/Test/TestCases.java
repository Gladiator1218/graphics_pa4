package Test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import Lights.*;
import Objects.*;
import Utils.ColorType;
import Utils.Material;
import Utils.Point3D;
import Utils.Quaternion;
import Utils.SketchBase;
import Utils.Vector3D;

public class TestCases {
    public boolean doGouranud;
    public boolean doPhong;
    public int Nsteps;
    public int ns;
    public Quaternion viewing_quaternion;
    public Vector3D viewing_center;
    public BufferedImage buff;
    public float[][] depthBuffer;
    public int materialOffset;
    public Vector3D centerOffset;
    public Quaternion rotateQuaternion;
    public boolean case4RotateSwitch;
    public BufferedImage bumpMap;
    public float scale;
    //light switches 
    public boolean ambientTermSwitch;
    public boolean diffuseTermSwitch;
    public boolean specularTermSwitch;
    public boolean ambientLightSwitch;
    public boolean infiniteLightSwitch;
    public boolean pointLightSwitch;
    public boolean spotLightSwitch;
    
    public Material materials[] = {
        Material.Mat1 , Material.Mat2 , Material.Mat3 , Material.Mat4 , Material.Mat5,
        Material.Mat6 , Material.Mat7 , Material.Mat8
    };
    public TestCases(
            boolean doSmooth , boolean doPhong, int Nsteps , int _ns , 
            Quaternion viewing_quaternion , Vector3D viewing_center , 
            BufferedImage buff , float[][] depthBuffer , int materialOffset,
            Vector3D _centerOffset, Quaternion _rotateQuaternion , boolean _case4RotateSwitch , float _scale)  {
        this.scale = _scale;
        this.doGouranud = doSmooth;
        this.doPhong = doPhong;
        this.Nsteps = Nsteps;
        this.ns = _ns ; 
        this.viewing_quaternion = viewing_quaternion ;
        this.viewing_center = viewing_center ;
        
        this.buff = buff ;
        this.depthBuffer = depthBuffer;
        this.materialOffset = materialOffset;
        this.centerOffset = new Vector3D(_centerOffset);
        this.rotateQuaternion = _rotateQuaternion;
        this.case4RotateSwitch = _case4RotateSwitch;
        
        try
        {
            bumpMap = ImageIO.read(new File("Orange-bumpmap.png"));
        } catch (IOException e)
        {
            System.out.println("Error: reading bumpMap image.");
        }
    }

    public void runTestCase0() 
    {
        // the simple example scene includes one sphere and one torus
        float radius = (float) 50.0;
        // Sphere3D sphere = new Sphere3D((float)128.0, (float)128.0,
        // (float)128.0, (float)1.5*radius, Nsteps, Nsteps);
//        Ellipsoid3D sphere = new Ellipsoid3D((float) 128.0, (float) 128.0, (float) 128.0, (float) 1.5 * radius,
//                (float) 0.5 * radius, (float) 2.5 * radius, Nsteps, Nsteps);
        Torus3D torus = new Torus3D((float) 256.0, (float) 384.0, (float) 128.0, (float) 0.8 * radius,
                (float) 1.25 * radius, Nsteps, Nsteps);
        Cylinder3D sphere = new Cylinder3D((float) 128.0, (float) 128.0, (float) 128.0, (float) 1.5 * radius,
                (float) 1.5 * radius, (float) 80, Nsteps, Nsteps);


        // view vector is defined along z axis
        // this example assumes simple othorgraphic projection
        // view vector is used in
        // (a) calculating specular lighting contribution
        // (b) backface culling / backface rejection
        Vector3D view_vector = new Vector3D((float) 0.0, (float) 0.0, (float) 1.0);

        // material properties for the sphere and torus
        // ambient, diffuse, and specular coefficients
        // specular exponent is a global variable
        ColorType torus_ka = new ColorType(0.0f, 0.0f, 0.0f);
        ColorType sphere_ka = new ColorType(0.0f, 0.0f, 0.0f);
        ColorType torus_kd = new ColorType(0.0f, 0.5f, 0.9f);
        ColorType sphere_kd = new ColorType(0.9f, 0.3f, 0.1f);
        ColorType torus_ks = new ColorType(1.0f, 1.0f, 1.0f);
        ColorType sphere_ks = new ColorType(1.0f, 1.0f, 1.0f);
        Material[] mats = { new Material(sphere_ka, sphere_kd, sphere_ks, ns, true, true),
                new Material(torus_ka, torus_kd, torus_ks, ns, true, true) };

        // define one infinite light source, with color = white
        ColorType light_color = new ColorType(1.0f, 1.0f, 1.0f);
        Vector3D light_direction = new Vector3D((float) 0.0, (float) (-1.0 / Math.sqrt(2.0)),
                (float) (1.0 / Math.sqrt(2.0)));
        light_direction.normalize();
        Vector3D light_position = new Vector3D((float) 0.0, (float) (1.0), (float) (0.0));
        InfiniteLight infLight = new InfiniteLight(light_color , light_direction , this.ambientLightSwitch , this.specularTermSwitch , this.diffuseTermSwitch);
        InfiniteLight light = infLight;
        // normal to the plane of a triangle
        // to be used in backface culling / backface rejection
        Vector3D triangle_normal = new Vector3D();

        // a triangle mesh
        Mesh3D mesh;

        int i, j, n, m;

        // temporary variables for triangle 3D vertices and 3D normals
        Vector3D v0, v1, v2, n0, n1, n2;

        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };

        for (int k = 0; k < 2; ++k) // loop twice: shade sphere, then torus
        {
            if (k == 0) {
                mesh = sphere.mesh;
                n = sphere.get_n();
                m = sphere.get_m();
            } else {
                mesh = torus.mesh;
                n = torus.get_n();
                m = torus.get_m();
            }

            // rotate the surface's 3D mesh using quaternion
            mesh.rotateMesh(viewing_quaternion, viewing_center);

            // draw triangles for the current surface, using vertex colors
            // this works for Gouraud and flat shading only (not Phong)
            for (i = 0; i < m - 1; ++i) {
                for (j = 0; j < n - 1; ++j) {
                    v0 = mesh.v[i][j];
                    v1 = mesh.v[i][j + 1];
                    v2 = mesh.v[i + 1][j + 1];
                    triangle_normal = computeTriangleNormal(v0, v1, v2);

                    if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                       // triangle?
                    {
                        if (doGouranud) {
                            // vertex colors for Gouraud shading
                            n0 = mesh.n[i][j];
                            n1 = mesh.n[i][j + 1];
                            n2 = mesh.n[i + 1][j + 1];
                            tri[0].c = light.applyLight(mats[k], view_vector, n0, v0);
                            tri[1].c = light.applyLight(mats[k], view_vector, n1, v1);
                            tri[2].c = light.applyLight(mats[k], view_vector, n2, v2);
                        } else {
                            // flat shading: use the normal to the triangle
                            // itself
                            n2 = n1 = n0 = triangle_normal;
                            tri[0].c = light.applyLight(mats[k], view_vector, triangle_normal, v0);
                            tri[1].c = light.applyLight(mats[k], view_vector, triangle_normal, v1);
                            tri[2].c = light.applyLight(mats[k], view_vector, triangle_normal, v2);
                        }

                        tri[0].x = (int) v0.x;
                        tri[0].y = (int) v0.y;
                        tri[0].z = (int) v0.z;
                        tri[1].x = (int) v1.x;
                        tri[1].y = (int) v1.y;
                        tri[1].z = (int) v1.z;
                        tri[2].x = (int) v2.x;
                        tri[2].y = (int) v2.y;
                        tri[2].z = (int) v2.z;

                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], doGouranud);
                    }

                    v0 = mesh.v[i][j];
                    v1 = mesh.v[i + 1][j + 1];
                    v2 = mesh.v[i + 1][j];
                    triangle_normal = computeTriangleNormal(v0, v1, v2);

                    if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                       // triangle?
                    {
                        if (doGouranud) {
                            // vertex colors for Gouraud shading
                            n0 = mesh.n[i][j];
                            n1 = mesh.n[i + 1][j + 1];
                            n2 = mesh.n[i + 1][j];
                            tri[0].c = light.applyLight(mats[k], view_vector, n0, v0);
                            tri[1].c = light.applyLight(mats[k], view_vector, n1, v1);
                            tri[2].c = light.applyLight(mats[k], view_vector, n2, v2);
                        } else {
                            // flat shading: use the normal to the triangle
                            // itself
                            n2 = n1 = n0 = triangle_normal;
                            tri[2].c = light.applyLight(mats[k], view_vector, triangle_normal, v2);
                            tri[1].c = light.applyLight(mats[k], view_vector, triangle_normal, v1);
                            tri[0].c = light.applyLight(mats[k], view_vector, triangle_normal, v0);
                        }

                        tri[0].x = (int) v0.x;
                        tri[0].y = (int) v0.y;
                        tri[0].z = (int) v0.z;
                        tri[1].x = (int) v1.x;
                        tri[1].y = (int) v1.y;
                        tri[1].z = (int) v1.z;
                        tri[2].x = (int) v2.x;
                        tri[2].y = (int) v2.y;
                        tri[2].z = (int) v2.z;

                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], doGouranud);
                    }
                }
            }
        }
    }

    

    public void runTestCase1() {
        ObjPainter painter = new ObjPainter(Nsteps , ns , viewing_quaternion , viewing_center , buff , depthBuffer);
        
        //define lights
        this.lightInit(painter);
        
        //draw object
        //torus
        Vector3D torusCenter = new Vector3D( 256.0f , 384.0f , 128.0f );
        torusCenter = torusCenter.plus(this.centerOffset);
        float torusRadius = 50.0f;
        painter.drawTorus3D(torusCenter, torusRadius , doGouranud, doPhong , this.materials[(0 + this.materialOffset) % 8] , this.rotateQuaternion , false);
        //ellipsoid
        Vector3D ellipsoidCenter = new Vector3D(128f , 200f , 256f);
        ellipsoidCenter = ellipsoidCenter.plus(this.centerOffset);
        float ellipsoidRadius = 50.0f;
        painter.drawEllipsoid3D(ellipsoidCenter, ellipsoidRadius, doGouranud, doPhong , this.materials[(1 + this.materialOffset) % 8]);
        //cylinder
        Vector3D cylinderCenter = new Vector3D(200f , 100f , 0f);
        cylinderCenter = cylinderCenter.plus(this.centerOffset);
        float cylinderRadius = 50.0f;
        float cylinderHeight = 90.0f;
        painter.drawCylinder3D(cylinderCenter, 1 * cylinderRadius , 2 * cylinderRadius , cylinderHeight, doGouranud, doPhong , this.materials[(2 + this.materialOffset) % 8]);
    }

    public void runTestCase2() {
        ObjPainter painter = new ObjPainter(Nsteps , ns , viewing_quaternion , viewing_center , buff , depthBuffer);
        
        //define lights
        this.lightInit(painter);
        
        //draw object
        //box
        Vector3D boxCenter = new Vector3D( 200.0f , 200.0f , 200.0f );
        boxCenter = boxCenter.plus(this.centerOffset);
        Vector3D boxSize = new Vector3D(80f , 100f , 60f);
        painter.drawBox3D(boxCenter, boxSize.x , boxSize.y , boxSize.z , doGouranud, doPhong , this.materials[(4 + this.materialOffset) % 8]);
        //superEllipsoid
        Vector3D superEllipsoidCenter = new Vector3D(328f , 400f , 256f);
        superEllipsoidCenter = superEllipsoidCenter.plus(this.centerOffset);
        Vector3D superEllipSize = new Vector3D(50f , 50f , 50f);
        painter.drawSuperEllipsoid3D(superEllipsoidCenter, superEllipSize, doGouranud, doPhong, this.materials[(1 + this.materialOffset) % 8], 2.5f , 2.5f);
        //superTorus
        Vector3D superToroidCenter = new Vector3D( 256.0f , 384.0f , 128.0f );
        superToroidCenter = superToroidCenter.plus(this.centerOffset);
        float superToroidRadius = 40f;
        float superToroidRAxial = 60f;
        painter.drawSuperToroid3D(superToroidCenter , superToroidRadius , superToroidRAxial , doGouranud , doPhong , this.materials[(0 + this.materialOffset) % 8] , 4f , 0.5f);
    }

    public void runTestCase3() {
        ObjPainter painter = new ObjPainter(Nsteps , ns , viewing_quaternion , viewing_center , buff , depthBuffer);
        //define lights
        this.lightInit(painter);
      //superTorus
        Vector3D superToroidCenter = new Vector3D( 256.0f , 384.0f , 128.0f );
        superToroidCenter = superToroidCenter.plus(this.centerOffset);
        float superToroidRadius = 40f;
        float superToroidRAxial = 60f;
        painter.drawSuperToroid3D(superToroidCenter , superToroidRadius , superToroidRAxial , doGouranud , doPhong , this.materials[(0 + this.materialOffset) % 8] , 1f , 1f);

        //torus
        Vector3D torusCenter = new Vector3D( 200.0f , 200.0f , 128.0f );
        torusCenter = torusCenter.plus(this.centerOffset);
        float torusRadius = 50.0f * scale;
        painter.drawTorus3D(torusCenter, torusRadius , doGouranud, doPhong , this.materials[(0 + this.materialOffset) % 8]  ,this.rotateQuaternion , case4RotateSwitch);
    }

    public void runTestCase4() {
        ObjPainter painter = new ObjPainter(Nsteps , ns , viewing_quaternion , viewing_center , buff , depthBuffer);
        //define lights
        this.lightInit(painter);
        
        //Bump Sphere
        Vector3D sphereCenter = new Vector3D( 200.0f , 200.0f , 128.0f );
        sphereCenter = sphereCenter.plus(this.centerOffset);
        float sphereRadius = 100.0f;
        boolean bump = true;

        //must use phong in Bump mapping and Nstep should be bigger 
        painter.drawSphere3D(sphereCenter, sphereRadius , doGouranud, true , this.materials[(0 + this.materialOffset) % 8]  , bump , this.bumpMap);
    }

    public void runTestCase5() {
        System.out.println("testCase:5");
    }

    public void runTestCase6() {
        System.out.println("testCase:6");
    }
    
    public void setTermSwitches(boolean a , boolean s , boolean d){
        this.ambientTermSwitch = a;
        this.specularTermSwitch = s;
        this.diffuseTermSwitch = d;
    }
    
    public void setLightSwitches(boolean a , boolean i , boolean p , boolean s){
        this.ambientLightSwitch = a;
        this.infiniteLightSwitch = i;
        this.pointLightSwitch = p;
        this.spotLightSwitch = s;
    }
    
    public void lightInit(ObjPainter painter){
        ColorType ambient_light_color = new ColorType(0.2f, 0.2f, 0.2f);
        ColorType light_color = new ColorType(1.0f, 1.0f, 1.0f);
        Vector3D light_direction = new Vector3D((float) 0.0, (float) (-1.0 / Math.sqrt(2.0)), (float) (1.0 / Math.sqrt(2.0)));
        light_direction.normalize();
        Vector3D light_position = new Vector3D((float) 0.0, (float) (1.0), (float) (0.0));
        
        AmbientLight ambientLight = new AmbientLight(ambient_light_color , this.ambientTermSwitch , this.specularTermSwitch , this.diffuseTermSwitch);
        InfiniteLight infLight = new InfiniteLight(light_color , light_direction , this.ambientTermSwitch , this.specularTermSwitch , this.diffuseTermSwitch);
        PointLight pointLight = new PointLight(light_color , light_position , this.ambientTermSwitch , this.specularTermSwitch , this.diffuseTermSwitch);
        
        light_position = new Vector3D(0f , 0f , 300f);
        light_direction = new Vector3D(0f , 0f , -1f);     
        SpotLight spotLight = new SpotLight(light_color, light_position, light_direction, 60, 5 , this.ambientLightSwitch , this.specularTermSwitch , this.diffuseTermSwitch);
        
        if(this.ambientLightSwitch)
            painter.addLight(ambientLight);
        if(this.infiniteLightSwitch)
            painter.addLight(infLight);
        if(this.pointLightSwitch)
            painter.addLight(pointLight);
        if(this.spotLightSwitch){
            painter.addLight(spotLight);
        }
    }
    
 // helper method that computes the unit normal to the plane of the triangle
    // degenerate triangles yield normal that is numerically zero
    private Vector3D computeTriangleNormal(Vector3D v0, Vector3D v1, Vector3D v2) {
        Vector3D e0 = v1.minus(v2);
        Vector3D e1 = v0.minus(v2);
        Vector3D norm = e0.crossProduct(e1);

        if (norm.magnitude() > 0.000001)
            norm.normalize();
        else // detect degenerate triangle and set its normal to zero
            norm.set((float) 0.0, (float) 0.0, (float) 0.0);

        return norm;
    }
}

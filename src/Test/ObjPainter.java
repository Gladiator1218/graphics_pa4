package Test;

import java.awt.image.BufferedImage;
import java.util.Vector;

import Lights.*;
import Objects.*;
import Utils.ColorType;
import Utils.Material;
import Utils.Point3D;
import Utils.Quaternion;
import Utils.SketchBase;
import Utils.Vector3D;

public class ObjPainter {
    public int Nsteps;
    public int ns;
    public Quaternion viewing_quaternion;
    public Vector3D viewing_center;
    public BufferedImage buff;
    public float[][] depthBuffer;
    public Vector<AmbientLight> ambientLights;
    public Vector<InfiniteLight> infiniteLights;
    public Vector<PointLight> pointLights;
    public Vector<SpotLight> spotLights;
    public ObjPainter(int Nsteps , int ns , Quaternion viewing_quaternion , Vector3D viewing_center , BufferedImage buff , float[][] depthBuffer)  {
        this.Nsteps = Nsteps;
        this.ns = ns ; 
        this.viewing_quaternion = viewing_quaternion ;
        this.viewing_center = viewing_center ;
        this.buff = buff ;
        this.depthBuffer = depthBuffer;
        ambientLights = new Vector<AmbientLight>();
        infiniteLights = new Vector<InfiniteLight>();
        pointLights = new Vector<PointLight>();
        spotLights = new Vector<SpotLight>();
    }

    //add light
    public void addLight(AmbientLight light){
        this.ambientLights.add(light);
//        System.out.println("ambientlight size:" + this.ambientLights.size());
    }
    public void addLight(PointLight light){
        this.pointLights.add(light);
//        System.out.println("pointlight size:" + this.pointLights.size());
    }
    public void addLight(InfiniteLight light){
        this.infiniteLights.add(light);
//        System.out.println("infinitelight size:" + this.infiniteLights.size());
    }
    public void addLight(SpotLight light){
        this.spotLights.add(light);
//        System.out.println("spotlight size:" + this.spotLights.size());
    }
    
    //remove light
    public void removeLight(AmbientLight light){
        this.ambientLights.remove(light);
    }
    public void removeLight(PointLight light){
        this.pointLights.remove(light);
    }
    public void removeLight(InfiniteLight light){
        this.infiniteLights.remove(light);
    }
    public void removeLight(SpotLight light){
        this.spotLights.remove(light);
    }
    
    //apply all lights to one point
    public ColorType applyLights(Material mat, Vector3D view_vector, Vector3D n, Vector3D objPosition){
        ColorType res = new ColorType();
        ColorType tempColor = new ColorType();
        //apply all lights in four vectors
        for(AmbientLight light : this.ambientLights){
            tempColor = light.applyLight(mat, view_vector, n, objPosition);
            res.r += (float)tempColor.r;
            res.g += (float)tempColor.g;
            res.b += (float)tempColor.b;
        }
        for(InfiniteLight light : this.infiniteLights){
            tempColor = light.applyLight(mat, view_vector, n, objPosition);
            res.r += (float)tempColor.r;
            res.g += (float)tempColor.g;
            res.b += (float)tempColor.b;
        }
        for(PointLight light : this.pointLights){
            tempColor = light.applyLight(mat, view_vector, n, objPosition);
            res.r += (float)tempColor.r;
            res.g += (float)tempColor.g;
            res.b += (float)tempColor.b;
        }
        for(SpotLight light : this.spotLights){
            tempColor = light.applyLight(mat, view_vector, n, objPosition);
            res.r += (float)tempColor.r;
            res.g += (float)tempColor.g;
            res.b += (float)tempColor.b;
        }
        res.r = (float) Math.min(1.0, res.r);
        res.g = (float) Math.min(1.0, res.g);
        res.b = (float) Math.min(1.0, res.b);
        return res;
    }
    
    public void drawSphere3D(Vector3D sphereCenter , float radius , boolean Gouraud , boolean Phong , Material SphereMat , boolean bump , BufferedImage bumpMap){
        int i , j;
        Vector3D view_vector = new Vector3D((float) 0.0, (float) 0.0, (float) 1.0);
        
        Sphere3D sphere = new Sphere3D(sphereCenter.x, sphereCenter.y, sphereCenter.z, radius, this.Nsteps, this.Nsteps);
        sphere.setBump(bump, bumpMap);
        Mesh3D mesh = sphere.mesh;
        int n = sphere.get_n();
        int m = sphere.get_m();
        Vector3D v0, v1, v2, n0, n1, n2;
        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        
        // rotate the surface's 3D mesh using quaternion
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        // draw triangles for the current surface, using vertex colors
        // this works for Gouraud and flat shading only (not Phong)
        for (i = 0; i < m - 1; ++i) {
            for (j = 0; j < n - 1; ++j) {
                v0 = mesh.v[i][j];
                v1 = mesh.v[i][j + 1];
                v2 = mesh.v[i + 1][j + 1];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i][j + 1];
                        n2 = mesh.n[i + 1][j + 1];
                        tri[0].c = this.applyLights(SphereMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(SphereMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(SphereMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(SphereMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(SphereMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(SphereMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j + 1]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , SphereMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }

                v0 = mesh.v[i][j];
                v1 = mesh.v[i + 1][j + 1];
                v2 = mesh.v[i + 1][j];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong){
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i + 1][j + 1];
                        n2 = mesh.n[i + 1][j];
                        tri[0].c = this.applyLights(SphereMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(SphereMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(SphereMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(SphereMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(SphereMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(SphereMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i + 1][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , SphereMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }
            }
        }
    }
    
    public void drawBox3D(Vector3D boxCenter , float length , float width , float height , boolean Gouraud , boolean Phong , Material boxMat){
        int i;
        Vector3D v0 , v1 , v2 , n0 , n1 , n2;
        Vector3D view_vector = new Vector3D(0f , 0f , 1f);
        Box3D box = new Box3D(boxCenter , length, width, height, 6 , 2);
        
        //draw box
        PlaneMesh3D mesh = box.mesh;
        int m = box.get_m();
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        
        //six faces
        for(i = 0 ; i < m ; i++)
        {
            //point 0 , 2 , 3
            v0 = mesh.v[i][0];
            v1 = mesh.v[i][2];
            v2 = mesh.v[i][3];

//            triangle_normal = computeTriangleNormal(v0, v1, v2);
            triangle_normal = mesh.n[i][0];
//            System.out.printf("normal 023(%.3f , %.3f , %.3f)\n" , triangle_normal.x , triangle_normal.y , triangle_normal.z);
            if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing triangle?
            {
//                System.out.printf("box first tri\n");
                if (Gouraud || Phong) {
                    n0 = mesh.n[i][0];
                    n1 = mesh.n[i][2];
                    n2 = mesh.n[i][3];
                    tri[0].c = this.applyLights(boxMat, view_vector, n0, v0);
                    tri[1].c = this.applyLights(boxMat, view_vector, n1, v1);
                    tri[2].c = this.applyLights(boxMat, view_vector, n2, v2);
                } else {
                    // flat shading: use the normal to the triangle itself
                    n2 = n1 = n0 = triangle_normal;
                    tri[0].c = this.applyLights(boxMat, view_vector, triangle_normal, v0);
                    tri[1].c = this.applyLights(boxMat, view_vector, triangle_normal, v1);
                    tri[2].c = this.applyLights(boxMat, view_vector, triangle_normal, v2);
                }
                tri[0].x = (int) v0.x;
                tri[0].y = (int) v0.y;
                tri[0].z = (int) v0.z;
                tri[1].x = (int) v1.x;
                tri[1].y = (int) v1.y;
                tri[1].z = (int) v1.z;
                tri[2].x = (int) v2.x;
                tri[2].y = (int) v2.y;
                tri[2].z = (int) v2.z;

                SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);            
                
                //point 0 , 4 ,5
                v0 = mesh.v[i][0];
                v1 = mesh.v[i][4];
                v2 = mesh.v[i][5];
//                System.out.printf("p0(%.3f , %.3f , %.3f)\n" , mesh.v[i][0].x , mesh.v[i][0].y , mesh.v[i][0].z);
//                System.out.printf("p1(%.3f , %.3f , %.3f)\n" , mesh.v[i][1].x , mesh.v[i][1].y , mesh.v[i][1].z);
//                System.out.printf("p2(%.3f , %.3f , %.3f)\n" , mesh.v[i][2].x , mesh.v[i][2].y , mesh.v[i][2].z);
//                System.out.printf("p3(%.3f , %.3f , %.3f)\n" , mesh.v[i][3].x , mesh.v[i][3].y , mesh.v[i][3].z);
//                System.out.printf("p4(%.3f , %.3f , %.3f)\n" , mesh.v[i][4].x , mesh.v[i][4].y , mesh.v[i][4].z);
//                System.out.printf("p5(%.3f , %.3f , %.3f)\n" , mesh.v[i][5].x , mesh.v[i][5].y , mesh.v[i][5].z);
//                System.out.printf("p6(%.3f , %.3f , %.3f)\n" , mesh.v[i][6].x , mesh.v[i][6].y , mesh.v[i][6].z);
//                System.out.printf("p7(%.3f , %.3f , %.3f)\n" , mesh.v[i][7].x , mesh.v[i][7].y , mesh.v[i][7].z);

                triangle_normal = mesh.n[i][0];

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][0];
                        n1 = mesh.n[i][4];
                        n2 = mesh.n[i][5];
                        tri[0].c = this.applyLights(boxMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(boxMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(boxMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(boxMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(boxMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(boxMat, view_vector, triangle_normal, v2);
                    }
                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;

                    SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                }
            }
        }
    }
    
    public void drawCylinderCaps3D(Vector3D cylinderCenter , float rx , float rz , float height , boolean isTopCap , boolean Gouraud , boolean Phong , Material CylinderCapMat){
        int j;
        Vector3D view_vector = new Vector3D(0f , 0f , 1f);
        //top cap
        Vector3D capCenter = new Vector3D(cylinderCenter.x , cylinderCenter.y , cylinderCenter.z);
        if(isTopCap){
            capCenter.y = capCenter.y + height / 2;
        }
        else{
            capCenter.y = capCenter.y - height / 2;
        }
        CylinderCap3D cap = new CylinderCap3D(capCenter , rx , rz , 1 , this.Nsteps , isTopCap);
        
        //draw cap
        PlaneMesh3D mesh = cap.mesh;
        int n = cap.get_n();
        Vector3D v0, v1, v2, n0, n1, n2;
        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        
        // rotate the surface's 3D mesh using quaternion
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        // draw triangles for the current surface, using vertex colors
        // this works for Gouraud and flat shading only (not Phong)
        for (j = 1; j < n - 1; ++j) {
            v0 = mesh.v[0][0];
            v1 = mesh.v[0][j];
            v2 = mesh.v[0][j + 1];
            triangle_normal = computeTriangleNormal(v0, v1, v2);

            if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                               // triangle?
            {
                    if(Gouraud || Phong) {
                    // vertex colors for Gouraud shading
                    n0 = mesh.n[0][0];
                    n1 = mesh.n[0][j];
                    n2 = mesh.n[0][j + 1];
                    tri[0].c = this.applyLights(CylinderCapMat, view_vector, n0, v0);
                    tri[1].c = this.applyLights(CylinderCapMat, view_vector, n1, v1);
                    tri[2].c = this.applyLights(CylinderCapMat, view_vector, n2, v2);
                } else {
                    // flat shading: use the normal to the triangle
                    // itself
                    n2 = n1 = n0 = triangle_normal;
                    tri[0].c = this.applyLights(CylinderCapMat, view_vector, triangle_normal, v0);
                    tri[1].c = this.applyLights(CylinderCapMat, view_vector, triangle_normal, v1);
                    tri[2].c = this.applyLights(CylinderCapMat, view_vector, triangle_normal, v2);
                }
                tri[0].x = (int) v0.x;
                tri[0].y = (int) v0.y;
                tri[0].z = (int) v0.z;
                tri[1].x = (int) v1.x;
                tri[1].y = (int) v1.y;
                tri[1].z = (int) v1.z;
                tri[2].x = (int) v2.x;
                tri[2].y = (int) v2.y;
                tri[2].z = (int) v2.z;

                SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
            }
        }
    }
    
    public void drawCylinder3D(Vector3D cylinderCenter , float rx , float rz , float height , boolean Gouraud , boolean Phong , Material CylinderMat){
        int i , j;
        Vector3D view_vector = new Vector3D((float) 0.0, (float) 0.0, (float) 1.0);
        
        Cylinder3D cylinder = new Cylinder3D(cylinderCenter.x, cylinderCenter.y, cylinderCenter.z, (float)rx,
                rz, height , Nsteps, Nsteps);
        
        Mesh3D mesh = cylinder.mesh;
        int n = cylinder.get_n();
        int m = cylinder.get_m();
        Vector3D v0, v1, v2, n0, n1, n2;
        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        
        // rotate the surface's 3D mesh using quaternion
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        // draw triangles for the current surface, using vertex colors
        // this works for Gouraud and flat shading only (not Phong)
        for (i = 0; i < m - 1; ++i) {
            for (j = 0; j < n - 1; ++j) {
                v0 = mesh.v[i][j];
                v1 = mesh.v[i][j + 1];
                v2 = mesh.v[i + 1][j + 1];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i][j + 1];
                        n2 = mesh.n[i + 1][j + 1];
                        tri[0].c = this.applyLights(CylinderMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(CylinderMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(CylinderMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(CylinderMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(CylinderMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(CylinderMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j + 1]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0], tri[1] , tri[2] , CylinderMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }

                v0 = mesh.v[i][j];
                v1 = mesh.v[i + 1][j + 1];
                v2 = mesh.v[i + 1][j];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i + 1][j + 1];
                        n2 = mesh.n[i + 1][j];
                        tri[0].c = this.applyLights(CylinderMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(CylinderMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(CylinderMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(CylinderMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(CylinderMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(CylinderMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i + 1][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , CylinderMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }
            }
        }
        
        boolean isTopCap = true;
        //draw topCap
        drawCylinderCaps3D(cylinderCenter , rx , rz , height , isTopCap , Gouraud , Phong , CylinderMat);
        //draw botCap
        drawCylinderCaps3D(cylinderCenter , rx , rz , height , !isTopCap , Gouraud , Phong , CylinderMat);
    }
    
    public void drawEllipsoid3D(Vector3D ellipsoidCenter , float radius , boolean Gouraud , boolean Phong , Material EllipsoidMat){
        int i , j;
        Vector3D view_vector = new Vector3D((float) 0.0, (float) 0.0, (float) 1.0);
        
        Ellipsoid3D ellipsoid = new Ellipsoid3D(ellipsoidCenter.x, ellipsoidCenter.y, ellipsoidCenter.z, (float) 0.8 * radius,
                (float) 1.25 * radius, (float)1.0 * radius , Nsteps, Nsteps);
        
        Mesh3D mesh = ellipsoid.mesh;
        int n = ellipsoid.get_n();
        int m = ellipsoid.get_m();
        Vector3D v0, v1, v2, n0, n1, n2;
        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        
        // rotate the surface's 3D mesh using quaternion
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        // draw triangles for the current surface, using vertex colors
        // this works for Gouraud and flat shading only (not Phong)
        for (i = 0; i < m - 1; ++i) {
            for (j = 0; j < n - 1; ++j) {
                v0 = mesh.v[i][j];
                v1 = mesh.v[i][j + 1];
                v2 = mesh.v[i + 1][j + 1];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i][j + 1];
                        n2 = mesh.n[i + 1][j + 1];
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j + 1]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , EllipsoidMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }

                v0 = mesh.v[i][j];
                v1 = mesh.v[i + 1][j + 1];
                v2 = mesh.v[i + 1][j];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong){
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i + 1][j + 1];
                        n2 = mesh.n[i + 1][j];
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i + 1][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , EllipsoidMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }
            }
        }
    }
    
    public void drawSuperEllipsoid3D(Vector3D ellipsoidCenter , Vector3D radius , boolean Gouraud , boolean Phong , Material EllipsoidMat , float s1 , float s2){
        int i , j;
        Vector3D view_vector = new Vector3D((float) 0.0, (float) 0.0, (float) 1.0);
        
        SuperEllipsoid3D ellipsoid = new SuperEllipsoid3D(ellipsoidCenter , radius.x ,
                radius.z , radius.y , s1 , s2 , Nsteps, Nsteps);
        
        Mesh3D mesh = ellipsoid.mesh;
        int n = ellipsoid.get_n();
        int m = ellipsoid.get_m();
        Vector3D v0, v1, v2, n0, n1, n2;
        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        
        // rotate the surface's 3D mesh using quaternion
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        // draw triangles for the current surface, using vertex colors
        // this works for Gouraud and flat shading only (not Phong)
        for (i = 0; i < m - 1; ++i) {
            for (j = 0; j < n - 1; ++j) {
                v0 = mesh.v[i][j];
                v1 = mesh.v[i][j + 1];
                v2 = mesh.v[i + 1][j + 1];
                triangle_normal = computeTriangleNormal(v0, v1, v2);
                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i][j + 1];
                        n2 = mesh.n[i + 1][j + 1];
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j + 1]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , EllipsoidMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }

                v0 = mesh.v[i][j];
                v1 = mesh.v[i + 1][j + 1];
                v2 = mesh.v[i + 1][j];
                triangle_normal = computeTriangleNormal(v0, v1, v2);
                
                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong){
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i + 1][j + 1];
                        n2 = mesh.n[i + 1][j];
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(EllipsoidMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i + 1][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , EllipsoidMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }
            }
        }
    }
    
    public void drawSuperToroid3D(Vector3D toroidCenter , float radius , float r_axial , boolean Gouraud , boolean Phong , Material ToroidMat , float s1 , float s2){
        int i , j;
        Vector3D view_vector = new Vector3D((float) 0.0, (float) 0.0, (float) 1.0);
        
        SuperTorus3D toroid = new  SuperTorus3D(toroidCenter , radius , r_axial, s1 , s2 , Nsteps , Nsteps);
        
        Mesh3D mesh = toroid.mesh;
        int n = toroid.get_n();
        int m = toroid.get_m();
        Vector3D v0, v1, v2, n0, n1, n2;
        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        
        // rotate the surface's 3D mesh using quaternion
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        // draw triangles for the current surface, using vertex colors
        // this works for Gouraud and flat shading only (not Phong)
        for (i = 0; i < m - 1; ++i) {
            for (j = 0; j < n - 1; ++j) {
                v0 = mesh.v[i][j];
                v1 = mesh.v[i][j + 1];
                v2 = mesh.v[i + 1][j + 1];
                triangle_normal = computeTriangleNormal(v0, v1, v2);
                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i][j + 1];
                        n2 = mesh.n[i + 1][j + 1];
                        tri[0].c = this.applyLights(ToroidMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(ToroidMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(ToroidMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(ToroidMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(ToroidMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(ToroidMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j + 1]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , ToroidMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }

                v0 = mesh.v[i][j];
                v1 = mesh.v[i + 1][j + 1];
                v2 = mesh.v[i + 1][j];
                triangle_normal = computeTriangleNormal(v0, v1, v2);
                
                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong){
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i + 1][j + 1];
                        n2 = mesh.n[i + 1][j];
                        tri[0].c = this.applyLights(ToroidMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(ToroidMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(ToroidMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(ToroidMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(ToroidMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(ToroidMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i + 1][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , ToroidMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }
            }
        }
    }
    
    public void drawTorus3D(Vector3D torusCenter , float radius  , boolean Gouraud , boolean Phong , Material torusMat , Quaternion rotateQuaternion , boolean rotate){
        int i , j;
        Vector3D view_vector = new Vector3D((float) 0.0, (float) 0.0, (float) 1.0);
        
        Torus3D torus = new Torus3D(torusCenter.x, torusCenter.y, torusCenter.z, (float) 0.8 * radius,
                (float) 1.25 * radius, Nsteps, Nsteps);
        Mesh3D mesh = torus.mesh;
        int n = torus.get_n();
        int m = torus.get_m();
        Vector3D v0, v1, v2, n0, n1, n2;
        // projected triangle, with vertex colors
        Point3D[] tri = { new Point3D(), new Point3D(), new Point3D() };
        Vector3D triangle_normal = new Vector3D();
        
        // rotate the surface's 3D mesh using quaternion
        if(rotate){
            mesh.rotateMesh(rotateQuaternion, torusCenter);
        }
        mesh.rotateMesh(viewing_quaternion, viewing_center);
        // draw triangles for the current surface, using vertex colors
        // this works for Gouraud and flat shading only (not Phong)
        for (i = 0; i < m - 1; ++i) {
            for (j = 0; j < n - 1; ++j) {
                v0 = mesh.v[i][j];
                v1 = mesh.v[i][j + 1];
                v2 = mesh.v[i + 1][j + 1];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong){
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i][j + 1];
                        n2 = mesh.n[i + 1][j + 1];
                        tri[0].c = this.applyLights(torusMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(torusMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(torusMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(torusMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(torusMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(torusMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j + 1]);

                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , torusMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }

                v0 = mesh.v[i][j];
                v1 = mesh.v[i + 1][j + 1];
                v2 = mesh.v[i + 1][j];
                triangle_normal = computeTriangleNormal(v0, v1, v2);

                if (view_vector.dotProduct(triangle_normal) > 0.0) // front-facing
                                                                   // triangle?
                {
                    if (Gouraud || Phong) {
                        // vertex colors for Gouraud shading
                        n0 = mesh.n[i][j];
                        n1 = mesh.n[i + 1][j + 1];
                        n2 = mesh.n[i + 1][j];
                        tri[0].c = this.applyLights(torusMat, view_vector, n0, v0);
                        tri[1].c = this.applyLights(torusMat, view_vector, n1, v1);
                        tri[2].c = this.applyLights(torusMat, view_vector, n2, v2);
                    } else {
                        // flat shading: use the normal to the triangle
                        // itself
                        n2 = n1 = n0 = triangle_normal;
                        tri[0].c = this.applyLights(torusMat, view_vector, triangle_normal, v0);
                        tri[1].c = this.applyLights(torusMat, view_vector, triangle_normal, v1);
                        tri[2].c = this.applyLights(torusMat, view_vector, triangle_normal, v2);
                    }

                    tri[0].x = (int) v0.x;
                    tri[0].y = (int) v0.y;
                    tri[0].z = (int) v0.z;
                    tri[0].n = new Vector3D(mesh.n[i][j]);
                    tri[1].x = (int) v1.x;
                    tri[1].y = (int) v1.y;
                    tri[1].z = (int) v1.z;
                    tri[1].n = new Vector3D(mesh.n[i + 1][j + 1]);
                    tri[2].x = (int) v2.x;
                    tri[2].y = (int) v2.y;
                    tri[2].z = (int) v2.z;
                    tri[2].n = new Vector3D(mesh.n[i + 1][j]);
                    if(Phong){
                        this.drawPhongTriangle(tri[0] , tri[1] , tri[2] , torusMat , view_vector);
                    }else{
                        SketchBase.drawTriangle(buff, depthBuffer, tri[0], tri[1], tri[2], Gouraud);
                    }
                }
            }
        }
    }
    
    private Vector3D computeTriangleNormal(Vector3D v0, Vector3D v1, Vector3D v2) {
        Vector3D e0 = v1.minus(v2);
        Vector3D e1 = v0.minus(v2);
        Vector3D norm = e0.crossProduct(e1);

        if (norm.magnitude() > 0.000001){
            norm.normalize();
        }
        else{
            norm.set((float) 0.0, (float) 0.0, (float) 0.0);
        }
        return norm;
    }

    public static void drawPoint(BufferedImage buff, float[][] depthBuff , Point3D p)
    {
        if(p.x>=0 && p.x<buff.getWidth() && p.y>=0 && p.y < buff.getHeight())
        {
            if(p.z > depthBuff[p.x][buff.getHeight()-p.y-1]){
                depthBuff[p.x][buff.getHeight()-p.y-1] = p.z;
                buff.setRGB(p.x, buff.getHeight()-p.y-1, p.c.getRGB_int());
            }
            
        }
    }

    public void drawPhongLine(Point3D p1, Point3D p2,
              Material mat, Vector3D view_vector) {
        int x0 = p1.x, y0 = p1.y;
        int xEnd = p2.x, yEnd = p2.y;
        int dx = Math.abs(xEnd - x0), dy = Math.abs(yEnd - y0); 
        Vector3D point = new Vector3D();
        Point3D p_tmp = new Point3D();
        if (dx == 0 && dy == 0) 
        {
            if (p1.y >= 0 && p1.y < buff.getHeight() && p1.x >= 0 && p1.x < buff.getWidth())
            {
                if (p1.z >= this.depthBuffer[p1.x][buff.getHeight() - p1.y - 1]) 
                {
                    this.depthBuffer[p1.x][buff.getHeight() - p1.y - 1] = p1.z;
                    point.x = (float) p1.x;
                    point.y = (float) p1.y;
                    point.z = p1.z;
                    p1.c = applyLights(mat, view_vector, p1.n, point);
                    buff.setRGB(p1.x, buff.getHeight()-p1.y-1, p1.c.getRGB_int());
                }
            }
            return;
        }

        boolean x_y_role_swapped = (dy > dx);
        if (x_y_role_swapped) {
            x0 = p1.y;
            y0 = p1.x;
            xEnd = p2.y;
            yEnd = p2.x;
            dx = Math.abs(xEnd - x0);
            dy = Math.abs(yEnd - y0);
        }

        int p = 2 * dy - dx;
        int twoDy = 2 * dy, twoDyMinusDx = 2 * (dy - dx);
        int x = x0, y = y0;

        int step_x = x0 < xEnd ? 1 : -1;
        int step_y = y0 < yEnd ? 1 : -1;

        float z0 = p1.z, zEnd = p2.z;
        Vector3D n0 = new Vector3D(p1.n), nEnd = new Vector3D(p2.n); 

        float dz = Math.abs(zEnd - z0);
        
        int step_z = z0 < zEnd ? 1 : -1;

        float whole_step_z = step_z * (dz / dx);
        float z = z0;
        Vector3D whole_step_n = new Vector3D((nEnd.x - n0.x) / dx,
                (nEnd.y - n0.y) / dx,
                (nEnd.z - n0.z) / dx);
        Vector3D n = new Vector3D(n0);
        
        // draw the first point
        if (x_y_role_swapped)
        {
            if (x >= 0 && x < buff.getHeight() && y >= 0 && y < buff.getWidth())
            {
                if (z >= this.depthBuffer[y][buff.getHeight() - x - 1]) 
                {
                    this.depthBuffer[y][buff.getHeight() - x - 1] = z;
                    
                    point.x = (float)y;
                    point.y = (float)x;
                    point.z = z;
                    p_tmp.x = y;
                    p_tmp.y = x;
                    
                    p_tmp.c = applyLights(mat, view_vector, n, point);
                    buff.setRGB(y, buff.getHeight()-x-1, p_tmp.c.getRGB_int());
                }
            }
        }
        else 
        {
            if (y >= 0 && y < buff.getHeight() && x >= 0 && x < buff.getWidth())
            {
                if (z >= this.depthBuffer[x][buff.getHeight() - y - 1])
                {
                    this.depthBuffer[x][buff.getHeight() - y - 1] = z;
                    
                    point.x = (float)x;
                    point.y = (float)y;
                    point.z = z;
                    p_tmp.x = x;
                    p_tmp.y = y;
                    p_tmp.c = applyLights(mat, view_vector, n, point);
                    buff.setRGB(x, buff.getHeight()-y-1, p_tmp.c.getRGB_int());
                }
            }
        }

        while (x != xEnd) {
            // increment x and y
            x += step_x;
            if (p < 0)
                p += twoDy;
            else {
                y += step_y;
                p += twoDyMinusDx;
            }

            /* handling for z-value */
            {
                z += whole_step_z;
            }
            {
                n = n.plus(whole_step_n);
            }
            if (x_y_role_swapped) 
            {
                if (x >= 0 && x < buff.getHeight() && y >= 0
                        && y < buff.getWidth()) 
                {
                    if (z >= this.depthBuffer[y][buff.getHeight() - x - 1])
                    {
                        this.depthBuffer[y][buff.getHeight() - x - 1] = z;
                        
                        point.x = (float)y;
                        point.y = (float)x;
                        point.z = z;
                        p_tmp.x = y;
                        p_tmp.y = x;
                        p_tmp.c = applyLights(mat, view_vector, n, point);
                        buff.setRGB(y, buff.getHeight()-x-1, p_tmp.c.getRGB_int());

                    }
                }
            } 
            else 
            {
                if (y >= 0 && y < buff.getHeight() && x >= 0
                        && x < buff.getWidth()) 
                {
                    if (z >= this.depthBuffer[x][buff.getHeight() - y - 1])
                    {
                        this.depthBuffer[x][buff.getHeight() - y - 1] = z;
                        
                        point.x = (float)x;
                        point.y = (float)y;
                        point.z = z;
                        p_tmp.x = x;
                        p_tmp.y = y;
                        p_tmp.c = applyLights(mat, view_vector, n, point);
                        buff.setRGB(x, buff.getHeight()-y-1, p_tmp.c.getRGB_int());
                    }
                }
            }
        }
    }

    public void drawPhongTriangle(Point3D p1, Point3D p2,
            Point3D p3, Material mat, Vector3D view_vector) 
    {
        // sort the triangle vertices by ascending x value
        // from small to large
        Point3D p[] = sortTriangleVerts(p1, p2, p3);
        int x;
        float y_a, y_b;
        float z_a, z_b;
        Vector3D n_a;
        Vector3D n_b;
        
        Point3D side_a = new Point3D(p[0]), side_b = new Point3D(p[0]);
        side_a.n = p[0].n;
        side_b.n = p[0].n;
        
        y_b = p[0].y;
        float dy_b = ((float) (p[2].y - p[0].y)) / (p[2].x - p[0].x);

        /* incremental for z-value */
        z_b = p[0].z;
        float dz_b = (p[2].z - p[0].z) / (p[2].x - p[0].x);
        Vector3D dn_b = new Vector3D((p[2].n.x - p[0].n.x) / (p[2].x - p[0].x),
                (p[2].n.y - p[0].n.y) / (p[2].x - p[0].x),
                (p[2].n.z - p[0].n.z) / (p[2].x - p[0].x));
        n_b = p[0].n;
        
        float dz_a = 0.0f;
        float dy_a;
        Vector3D dn_a = new Vector3D();

        if (p[0].x != p[1].x) {
            y_a = p[0].y;
            z_a = p[0].z;
            n_a = p[0].n;
            
            dy_a = ((float) (p[1].y - p[0].y)) / (p[1].x - p[0].x);

            /* incremental for z-value */
            dz_a = (p[1].z - p[0].z) / (p[1].x - p[0].x);
            dn_a = new Vector3D((p[1].n.x - p[0].n.x) / (p[1].x - p[0].x),
                                (p[1].n.y - p[0].n.y) / (p[1].x - p[0].x),
                                (p[1].n.z - p[0].n.z) / (p[1].x - p[0].x));

            for (x = p[0].x; x < p[1].x; ++x) {
                this.drawPhongLine(side_a, side_b, mat, view_vector);

                ++side_a.x;
                ++side_b.x;

                y_a += dy_a;
                y_b += dy_b;
                {
                    z_a += dz_a;
                    z_b += dz_b;
                }
                {
                    n_a = n_a.plus(dn_a);
                    n_b = n_b.plus(dn_b);
                }
                side_a.y = (int) y_a;
                side_b.y = (int) y_b;
                {
                    side_a.z = z_a;
                    side_b.z = z_b;
                }
                {
                    side_a.n = n_a;
                    side_b.n = n_b;
                }
            }
        }

        // there is no left-hand part of triangle
        if (p[1].x == p[2].x)
            return;

        // set up to fill the left-hand part of triangle
        // replace segment a
        side_a = new Point3D(p[1]);
        side_a.n = p[1].n;
        
        y_a = p[1].y;
        dy_a = ((float) (p[2].y - p[1].y)) / (p[2].x - p[1].x);

        /* do the same thing for z-value */
        {
            z_a = p[1].z;
            dz_a = (p[2].z - p[1].z) / (p[2].x - p[1].x);
        }
        {
            n_a = p[1].n;
            dn_a = new Vector3D((p[2].n.x - p[1].n.x) / (p[2].x - p[1].x),
                    (p[2].n.y - p[1].n.y) / (p[2].x - p[1].x),
                    (p[2].n.z - p[1].n.z) / (p[2].x - p[1].x));
        }
        // loop over the columns for left-hand part of triangle
        // filling from side a to side b of the span
        for (x = p[1].x; x <= p[2].x; ++x) {
            this.drawPhongLine(side_a, side_b, mat, view_vector);

            ++side_a.x;
            ++side_b.x;

            y_a += dy_a;
            y_b += dy_b;
            {
                z_a += dz_a;
                z_b += dz_b;
            }
            {
                n_a = n_a.plus(dn_a);
                n_b = n_b.plus(dn_b);
            }
            side_a.y = (int) y_a;
            side_b.y = (int) y_b;
            {
                side_a.z = z_a;
                side_b.z = z_b;
            }
            {
                side_a.n = n_a;
                side_b.n = n_b;
            }
        }
    }

    private static Point3D[] sortTriangleVerts(Point3D p1, Point3D p2, Point3D p3) {
        Point3D pts[] = { p1, p2, p3 };
        Point3D tmp;
        int j = 0;
        boolean swapped = true;

        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < 3 - j; i++) {
                if (pts[i].x > pts[i + 1].x) {
                    tmp = pts[i];
                    pts[i] = pts[i + 1];
                    pts[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
        return (pts);
    }
}
